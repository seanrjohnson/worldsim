//
// Just a shell for now, will eventually manage a connection to mongoDB hopefully
//
//

var ID = 0;
var db = {}; //provisional
var turn = 1;
var queue = [];

function storeRecord(data, callback)//storeRecord(type, data, callback)
{
	//console.log("6");
	error = 0;
	if (!(turn in db))
	{
		db[turn] = {};
	}
	var recID = ID;
	ID++;
	db[turn][recID] = {type: data.type, data: data};
	
	callback(error, recID, data);
}

exports.storeRecord = storeRecord;


function modifyRecord(pTurn, pID, type, data, callback)
{
	error = 0;
	if ((pTurn in db) && (pID in db[turn] ))
	{
		db[pTurn][pID] = {type: type, data: data};
	}
	
	callback(error, recID, type, data);
}

function modifyRecordField(pTurn, pID, type, field, value, callback)
{

}

function removeRecord(pTurn, pID, type, callback)
{
	error = 0;
	var owners = [];
	if ( db[turn][pID].type == type)
	{
		owners.push(db[turn][pID].data.sender);
		delete db[turn][pID];
	}
	else
	{
		error = 1;
	}
	callback(error, pID, type, owners);
}

function queueRequest(type, data, callback)
{
	console.log("2");
	queue.push({type: type, data: data, callback: callback});
	if (queue.length == 1) //if what was just pushed on there is the only thing on the queue
	{
		processQueue();
	}
}
exports.queueRequest = queueRequest;

function processQueue()
{
	console.log("3");
	if (queue.length > 0)
	{
		var command = queue.shift();
		
		if (validateRequest(command.type, command.data))
		{
			processRequest (command.type, command.data, command.callback);
		}
		else
		{
			command.callback(1, '',''); //there was an error, don't do anything
		}
		processQueue(); //go to next command
	}
}

//make sure request makes sense in the context of what's in the database
// this is difficult without an actual, functioning database...
function validateRequest(type, data)
{
	console.log("4");
	var out = false;
	switch(type)
	{
		case 'deletePolicy':
			if (db[turn][data.ID].type = 'policy') // && data.sender owns the policy (so they don't go deleting other people's policies...)
			{
				out = true;
			}
//			db[turn][recID] = {type: data.type, data: data};
			break;
		default:
			out = true; //TODO, get rid of this or change it to false
			break;
	}
	return out;
}

function processRequest (type, data, callback)
{
	console.log("5");
	switch(type)
	{
		case 'submitPolicy':
			storeRecord(data, callback);
			break;
		case 'deletePolicy':
			removeRecord(turn, data.ID, 'policy', callback);
			break;
	}
}