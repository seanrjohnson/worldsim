//
//
var opts = {'mode' : 'http'}; //for nodejitsu, always use http
var http;
if (opts.mode == 'https'){
	http = require('https');
} else {
	http = require('http');
}

var mime = require('mime');
var querystring = require("querystring");
var fs = require('fs');
var urlparse = require('url');
var initialized = false;
// var db = require('mongoose');

////// uncomment if using https
// var HTTPSoptions = {
	// key: fs.readFileSync('key.pem'),
	// cert: fs.readFileSync('cert.pem')
// };

var app;
if (opts.mode == 'https'){
	app = http.createServer(HTTPSoptions, handler);
} else {
	app = http.createServer(handler);
}

if (opts.mode == 'https') {
	app.listen(443); 
} else {
	//app.listen(80);
	app.listen(8080);
} 

io = require('socket.io').listen(app);

var servRoot = __dirname + '/client';
var chatLog = [];

var adminOnly = hashify(['/secret.js','/admin.html','adminViews.js'],1);
var everyone = hashify(['/style.css','/index.html','/authenticate.html','/'],1);
//if not in one of the above two lists a resource is only available authenticated users, regardless of whether they have admin priveliges or not

game = require('./gameData.js');
users = require('./userData.js');
game.initialize(function (err) {
	if (!err)
	{
		initialized = true;
	}
});

//users.initialize();

function checkPermission(path,name,code)
{
	out = false;
	//console.log('y');
	if(path in everyone)
	{
		out = true;
	}
	else if (path in adminOnly && users.isAdmin(name) && users.checkCode(name,code))
	{
		out = true;
	}
	else if (users.checkCode(name,code))
	{
		out = true;
	}
	return out;
}

function handler (req, res) {
	
	//console.log(req.url);
	//console.log(req.headers);
	//console.log('');
	
	//var cookieString = "";
	var cookies = {};

	if ('cookie' in req.headers) //get cookies
	{
		cookies = parseCookies(req.headers.cookie);
	}
	
	var urlPath = urlparse.parse(req.url).pathname;
	var urlGet = urlparse.parse(req.url, true).query;
	var postData = "";
	req.addListener("data", function(postDataChunk) {
		postData += postDataChunk;
		//console.log("Received POST data chunk '"+ postDataChunk + "'.");
	});
	//console.log(cookieString);
	//console.log(cookies);
	//console.log(urlPath);
	req.addListener("end", function() { // don't process request until all GET and POST and Cookie data has been received
		var post = querystring.parse(postData);
		if (checkPermission(urlPath,cookies.username,cookies.code) && initialized)  //don't serve any data unless game data is initialized, and the client has proper permissions.
		{
			if (urlPath == "/authenticate.html")
			{
				if (users.checkCode(cookies.username, cookies.code)) //user already authenticated
				{
					var dest = 'game.html';
					if (users.isAdmin(cookies.username))
					{
						dest = 'admin.html';
					}
					res.writeHead(303, { "Location": dest});
					res.end();
				}
				else if (!post["username"] || !(users.exists(post["username"]))){  //no username sent, or invalid username sent
					//console.log("Failed login attempt");
					//console.log("Username:"+post["username"]);
					//console.log("Password:"+post["password"]);
					//console.log("Expected:"+passwords[post["username"]]);
					res.writeHead(303, { "Location": 'index.html?r=1'});
					res.end();
				}
				else if (users.getLogonState(post["username"]) != 'off') //user logged in
				{
					res.writeHead(303, { "Location": 'index.html?r=2'});
					res.end();
				}
				else if (users.checkPassword(post["username"],post["password"])) //password correct, send them a Code
				{
					var code = rehash(post["username"]);
					var dest = 'game.html';
					//console.log(post["username"]);
					//console.log(users.isAdmin(post["username"]));
					if (users.isAdmin(post["username"]))
					{
						dest = 'admin.html';
					}
					res.writeHead(303, { "Location": dest,
					"Set-Cookie": ["username="+post["username"],"code="+code]
					});
					// note: cookie setting is an iterative command so you pass cookies in as an array with each member being a cookie setting line
					
					users.approve(post["username"],code);
					res.end();
				}
				else //only other possibility is that the password is wrong
				{
					res.writeHead(303, { "Location": 'index.html?r=0'});
					res.end();
				}
			} else if (urlPath == "/game.html") {
				res.writeHead(200);
				loadGamePage(res);
			} else if (urlPath == "/admin.html") {
				res.writeHead(200);
				loadAdminPage(res);
			} else if (urlPath == "/index.html" || urlPath == "/") {
				if (users.checkCode(cookies.username, cookies.code)) //if they've already been authenticated
				{
					res.writeHead(303, { "Location": 'authenticate.html'}); //authenticate.html will redirect them to the right game page (game.html or admin.html)
					res.end();
				}
				else if ('r' in urlGet && !isNaN(urlGet.r))
				{
					loadMainPage(res, urlGet.r);
				}
				else
				{
					loadMainPage(res);
				}
			} else {
				var file = urlPath;
				fs.readFile(servRoot + file, function (err, data) {
					if (err) 
					{
						res.writeHead(404);
						res.write("404: file not found");
					}
					else 
					{
						res.writeHead(200, {
						"Content-Type":mime.lookup(servRoot+file)
						});
						res.write(data);
					}
					res.end();
				});
			}
		}
		else //if authentication is wrong, ask user to go back and reauhtenticate
		{
			if (urlPath == "/game.html" || urlPath == "/admin.html")
			{
				res.writeHead(303, { "Location": '/'});
				res.end();
			}
			else
			{
				res.writeHead(401);
				res.write("401: Resource unauthorized, Please go back to the Logon Page and log in");
				res.write('<br><a href="/"> Logon Page </a>');
				res.end();
			}
		}
	});
}

//function redirectMainPage
function loadMainPage(res, errorCode){
	var file = "/index.html";
	//                       0                     1                       2                     3
	var errorCodes = ['Incorrect Password', 'Invalid Username', 'User already logged on', 'Logged off'];
	var toInterpolate = {};
	if (errorCode != undefined && errorCode < errorCodes.length)
	{
		toInterpolate.errorMsg = errorCodes[errorCode];
	}
	else
	{
		toInterpolate.errorMsg = "";
	}
	
	fs.readFile(servRoot + file, 'utf8', function (err, data) {
		if (err) {
			res.writeHead(500);
			res.write('Internal Server Error: Please contact the site administrator');
		}
		else
		{
			res.writeHead(200, {
			//"Content-Type":"text/html"
			});
			
			res.write(interpolateHTML(data, toInterpolate));
		}
		res.end();
	});
}

function loadGamePage(res) 
{
	fs.readFile(servRoot + '/chat.html', 'utf8', function (err1, chat) {
		if (err1)
		{
			res.writeHead(500);
			res.write('Internal Server Error: Please contact the site administrator');
		}
		else
		{
			fs.readFile(servRoot + '/game.html', 'utf8', function (err, game) {
				if (err)
				{
					res.writeHead(500);
					res.write('Internal Server Error: Please contact the site administrator');
				}
				else
				{
					var out = interpolateHTML(game, {chatArea: chat});
					res.write(out);
				}
				res.end();
			});
		}
	});
}

function loadAdminPage(res)
{
	fs.readFile(servRoot + '/chat.html', 'utf8', function (err1, chat) {
		if (err1)
		{
			res.writeHead(500);
			res.write('Internal Server Error: Please contact the site administrator');
		}
		else
		{
			fs.readFile(servRoot + '/admin.html', 'utf8', function (err, game) {
				if (err)
				{
					res.writeHead(500);
					res.write('Internal Server Error: Please contact the site administrator');
				}
				else
				{
					var out = interpolateHTML(game, {chatArea: chat});
					res.write(out);
				}
				res.end();
			});
		}
	});
}


//
// Function to authorize incoming socket.io connection requests
//
// kindly modified from: https://github.com/LearnBoost/socket.io/wiki/Authorizing
// see also https://github.com/LearnBoost/Socket.IO/wiki/Configuring-Socket.IO
io.configure(function(){
	io.set('log level', 1);
	io.set('authorization', function(handshakeData, callback) {
		//console.log(handshakeData);
		var cookieString = "";
		var cookies = {};
		var authorized = false;
		if ('cookie' in handshakeData.headers) //get cookies
		{
			cookies = parseCookies(handshakeData.headers.cookie);
		}
		//console.log(cookieString);
		//console.log(cookies);
		if ('username' in cookies && 'code' in cookies)
		{
			//name and code correct, logonState valid, and number of open sockets below maximum
			if (users.checkCode(cookies.username,cookies.code) && users.getLogonState(cookies.username) != "off" && users.connectionsBelowMax(cookies.username)) 
			{
				authorized = true;
			}
		}
		callback(null,authorized);
	});
});

//if it gets this far, it's already been authorized
io.sockets.on('connection', function (socket) {
	//console.log(socket);
	
	///////////get authentication cookies
	var cookieString = "";
	var cookies = {};

	if ('cookie' in socket.handshake.headers) //get cookies
	{
		cookies = parseCookies(socket.handshake.headers.cookie);
	}
	socket.set('name', cookies.username);
	/////////////////////////
	
	//update userData
	users.connected(cookies.username,socket);
	
	
	
	
	//// Functions to handle logging out and disconnecting ////
	socket.on('logout', function() {
		socket.get('name', function (err, name) {
			users.logout(name);
		});
	});
	
	socket.on('disconnect', function (data) {
		socket.get('name', function (err, name) {
			//console.log(name);
			users.disconnected(name,socket);
		});
	});
	


	// function getValue(sock, name) //I know... I know... I'm doing it wrong...
	// {
		// var out;
		// sock.get(name, function (err, nombre) {
			// out = nombre;
		// });
	// return out;
	// }

});

/*
*
* turn an array into a hash with keys as the array members and values as val
*
*/
function hashify(arr,val)
{
	var out = {};
	for (var i = 0; i < arr.length; i++)
	{
		out[arr[i]] = val;
	}
	return out;
}

function rehash(name){
	function stringHash(word){
		var hash = 0;
		if (word.length == 0) return hash;
		for (i = 0; i < word.length; i++) {
			chara = word.charCodeAt(i);
			hash = ((hash<<5)-hash)+chara;
			hash = hash & hash; // Convert to 32bit integer
		}
		return hash;
	}


	var currentTime = new Date();
	return currentTime.getTime()*stringHash(name)%68485165865173;
}


// replaces html comments with values from the associative array vars
// example: if vars = {errorMsg: 'Incorrect Password'}
// then <!-- errorMsg --> would be replaced with 'Incorrect Password'
// note: multiline comments are ignored, only one interpolation per comment, whitespace trimmed from comment contents 
function interpolateHTML(html, vars)
{
	var out = html;
	var comments = html.match(/<\!--(.*?)-->?/g);
	//console.log(comments);
	for (var i = 0; i < comments.length; i++)
	{
		var contents = comments[i].match(/<\!--(.*)-->/)[1].trim(); //find text between comment flags and trim it
		//console.log(contents);
		if (contents in vars)
		{
			out = out.replace(comments[i],vars[contents]);
		}
	}
	return out;
}

function parseCookies(cookieString)
{
	var splitCookies = cookieString.split(';');
	var cookies = {};
	var tmpCookie = [];
	for (var i = 0; i < splitCookies.length; i++)
	{
		tmpCookie = splitCookies[i].split('=');
		cookies[tmpCookie[0].trim()] = tmpCookie[1].trim();
	}
	return cookies;
}
