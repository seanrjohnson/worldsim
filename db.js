//
// Using an internal, object based system.
// Use this as a template for implementing other database interfaces (i.e. implement all these methods)
//

var ID = 0;
var db = {};
db.state = {}; // country specific game data in the form: state[turn][recID] = {type: recordType, data: record}
db.users = {}; //place for user-specific data
db.game = {}; // meta-game data, such as when the turn ends
db.client = {}; //place to allow clients to save data to 
var turn = 1;  //It's pretty silly to keep track of turn with this variable and with db.game.turn, This variable should be eliminated!
//var queue = [];
var fs = require('fs');

//returns all data in the 'state' object, in the format: out[turn][id] = [type, data];
function getState(callback)
{
	err = null;
	callback(err, db.state);
}
exports.getState = getState;

function storeRecord(data, callback)//storeRecord(type, data, callback)
{
	storeRecordTurn(turn, data, callback);
}
exports.storeRecord = storeRecord;

function storeRecordTurn(pTurn, data, callback)
{
	//console.log("6");
	var err = null;
	if (!(turn in db.state))
	{
		db.state[pTurn] = {};
	}
	var recID = ID;
	ID++;
	db.state[pTurn][recID] = {type: data.type, data: data};
	
	if (err == null)
	{
		backupDatabase( function () {
		callback(err, recID, data)
		});
	}
	else
	{
		callback(err, recID, data);
	}
}
exports.storeRecordTurn = storeRecordTurn;

function replaceRecord(pID, type, data, callback)
{
	replaceRecordTurn(turn, pID, type, data, callback);
}

exports.replaceRecord = replaceRecord;

function replaceRecordTurn(pTurn, pID, type, data, callback)
{
	err = null;
	if ((pTurn in db.state) && (pID in db.state[turn] ))
	{
		db.state[pTurn][pID] = {type: type, data: data};
	}
	else
	{
		err = new Error('record ' + pID + ' for turn ' + pTurn + ' not found'); 
	}

	if (err == null)
	{
		backupDatabase( function () {
		callback(err, pID, type, data);
		});
	}
	else
	{
		callback(err, pID, type, data);
	}
}

function modifyRecordField(pID, type, field, value, callback)
{
	modifyRecordFieldTurn(turn, pID, type, field, value, callback);
}

exports.modifyRecordField = modifyRecordField;

function modifyRecordFieldTurn(pTurn, pID, type, field, value, callback)
{
	var err = null;
	if ((pTurn in db.state) && (pID in db.state[turn]) && ('data' in db.state[turn][pID]) && (field in db.state[turn][pID].data))
	{
		db.state[pTurn][pID].data[field] = value;
	}
	else
	{
		err = new Error('record ' + pID + ' for turn ' + pTurn + ' not found'); 
	}

	if (err == null)
	{
		backupDatabase( function () {
		callback(err, type, pID, field, value);
		});
	}
	else
	{
		callback(err, type, pID, field, value);
	}
	
}

function removeRecord(pID, type, callback)
{
	removeRecordTurn(turn, pID, type, callback);
}
exports.removeRecord = removeRecord;

function removeRecordTurn(pTurn, pID, type, callback)
{
	var err = null;
	var owners = [];
	if ( db.state[turn][pID].type == type)
	{
		owners = db.state[turn][pID].data.targets;
		//owners.push(db[turn][pID].data.sender);
		delete db.state[turn][pID];
	}
	else
	{
		err = new Error("Could not remove record type: " + type + " ID: " + pID );
	}
	
	if (err == null)
	{
		backupDatabase( function () {
		callback(err, pID, type, owners);
		});
	}
	else
	{
		callback(err, pID, type, owners);
	}
}

//resets the state table for a given turn
function resetStateTurn(pTurn,callback)
{
	db.state[pTurn] = {};

	backupDatabase(function () {
		callback();
	});

}
exports.resetStateTurn = resetStateTurn;


// add data to a table that isn't the state table
// table is a string, data is an arbitrary data structure
function setTable(table, data, callback)
{
	var err = null;
	if (table == 'state')
	{
		err = new Error('set Table cannot be used with the -state- table');
	}
	else
	{
		if (!(table in db))
		{
			db[table] = {};
		}
		db[table] = data
	}
	
	if (err == null)
	{
		backupDatabase( function () {
		callback(err);
		});
	}
	else
	{
		callback(err);
	}

}
exports.setTable = setTable;


//path is an array containing a key path to data to be returned
function get(path,callback)
{
	var err = null;
	var out = {};
	var tmpTbl = db;
	for (i = 0; i < path.length -1; i++)
	{
		if (path[i] in tmpTbl)
		{
			tmpTbl = tmpTbl[path[i]];
		}
		else
		{
			err = new Error('Database record not found at' + path.join);
		}
	}

	if ((path[path.length -1] in tmpTbl))
	{
		out = tmpTbl[path[path.length - 1]];
	}
	callback(err, out);
}
exports.get = get;


//table is a string specifying the database table, path is an array specifying the key path to the value in the table, value is the value to be stored there
function modifyTable(table, path, value, callback)
{
	var err = null;
	if (table == 'state')
	{
		err = new Error('Error: modifyTable cannot be used with the -state- table');
	}
	else
	{
		if (!(table in db))
		{
			db[table] = {};
		}
		var tmpTbl = db[table];
		if (path.length > 0)
		{
			for (var i = 0; i < path.length-1; i++)
			{
				if (!(path[i] in tmpTbl))
				{
					tmpTbl[path[i]] = {};
				}
				tmpTbl = tmpTbl[path[i]];
			}
			tmpTbl[path[path.length-1]] = value;
		}
		else
		{
			err = new Error('Error: no path provided to modifyTable');
		}
	}
	if (err == null)
	{
		backupDatabase( function () {
		callback(err);
		});
	}
	else
	{
		callback(err);
	}

}
exports.modifyTable = modifyTable;

//remove data from a table that isn't the state table
//table is a string specifying the database table, path is an array specifying the key path to the value in the table
function remove(table, path, callback) 
{
	var err = null;
	if (table == 'state')
	{
		err = new Error('Error: db.remove cannot be used with the -state- table');
	}
	else if (!(table in db))
	{
		err = new Error('Error: table ' + table + ' does not exist, cannot remove data from it');
	}
	else
	{
		var tmpTbl = db[table];
		if (path.length > 0)
		{
			for (var i = 0; i < path.length-1; i++)
			{
				if (!(path[i] in tmpTbl))
				{
					err = new Error('Error: path ' + table + " "+  path.join + ' does not exist, cannot remove data from it');
					break;
				}
				tmpTbl = tmpTbl[path[i]];
			}
			if (!err)
			{
				delete tmpTbl[path[path.length-1]];
			}
		}
		else
		{
			err = new Error('Error: no path provided to db.remove');
		}
	}
	if (err == null)
	{
		backupDatabase( function () {
		callback(err);
		});
	}
	else
	{
		callback(err);
	}

}
exports.remove = remove;

function setTurn(pTurn)
{
	var out = false;
	if (!(isNaN(pTurn)))
	{
		turn = pTurn;
		out = true;
	}
	ID = highestID() + 1;
	return out;
}
exports.setTurn = setTurn;

function saveToFile(filename, callback)
{
	fs.writeFile(filename, JSON.stringify(db), 'utf8',callback);
}
exports.saveToFile = saveToFile;

function getDBTextDump(callback)
{

}


function loadFromFile(filename)
{
	var file = fs.readFileSync(filename,'utf8');
	if (file.length > 0)
	{
		db = JSON.parse(file);
	}
	ID = highestID() + 1;
	//console.log(db);
}
exports.loadFromFile = loadFromFile;


// Find the largest ID from the current turn
function highestID()
{
	var out = 0;
	if (turn in db.state)
	{
		var IDs = Object.keys(db.state[turn]).sort(function(a,b) { //sort from largest to smallest
		return parseInt(b)-parseInt(a);
		});
		if (IDs.length > 0)
		{
			out = parseInt(IDs[0]);
		}
	}
	return out;
}


function backupDatabase(callback)
{
	saveToFile('autobackup.txt',callback);
}
