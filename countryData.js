// modeled after userData.js, used for storing country data
// more than one user can share the same country, so users and countries must be kept distinct
//

var countries = {};

//stolen from http://stackoverflow.com/questions/5625569/include-external-js-file-in-node-js-app
// need to include wsClientState and json2, the same way that a browser would
var fs = require('fs');
var vm = require('vm');

// var path = ('./client/json2.js');
// var code = fs.readFileSync(path);
// vm.runInThisContext(code, path);

var path = ('./client/wsClientState.js');
var code = fs.readFileSync(path);
vm.runInThisContext(code, path);




exports.initialize = function (names)
{
	for (var i = 0; i < names.length; i++)
	{
		new country(names[i]);
	}
};


function country(name)
{
	this.state = new wsClientState;
	//this.state.country = name;
	countries[name] = this;
	//this.loadSampleData();
}

function resetStates()
{
	for (name in countries)
	{
		countries[name].state = new wsClientState;
	}
}
exports.resetStates = resetStates;

country.prototype = {

	//methods go here
	loadSampleData: function()
	{
		
		//csResource(targets, species, current, start, minimum, createCost, liquidateCost, canSell, canBuy, canLiquidate, canCreate)
	}
};

//Returns the names of all of the countries.
function getNames(){
	return Object.keys(countries);
}
//returns the state object for the country specified by "name"
function getState(name)
{
	if (exists(name))
	{
		return countries[name].state;
	}
	else
	{
		return {};
	}
}
exports.getState = getState;

function exists(name)
{
	return (name in countries);
};

exports.exists = exists;

// returns a hash where the keys are country names and the values are empty arrays
// originally written so that the arrays can be filled with usernames associated with the country
exports.hashOfArrays = function()
{
	var out = {};
	for (var key in countries)
	{
		out[key] = [];
	}
	return out;
};
