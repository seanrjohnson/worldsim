// A file where the server can keep track of the game state
// should be integrated with the database to make it robust against crashes (so if there's a crash you can pick up where you left off)


var db = require('./db.js');
//var db = require('./db_mongo.js'); //database interface

// var countries = require('./countryData.js');
// var countryNames = [];
var users = require('./userData');


var game = {locked: false, turn: 1, turnEnds: ""};            ; //true if the turn is over and certain types of requests should be refused.

var usersSettings = {}; //TODO: comment the next declaration out, and make this one the used one
// var usersSettings = {Sean: {password: "", country: "Admin", playername: "Sean J", abbr: "ad1", admin: true},
			// Player1: {password: "", country: "Fridujo", playername: "Player O", abbr: "fr1", admin: false},
			// Player2: {password: "", country: "Zumiland", playername: "Player T", abbr: "Zl1", admin: false},
			// Player3: {password: "", country: "Procyon", playername: "Player Th", abbr: "Pc1", admin: false},
			// Player4: {password: "", country: "Lotor", playername: "Player F", abbr: "Lt1", admin: false},
			// Player5: {password: "", country: "Fridujo", playername: "Player Fi", abbr: "fr2", admin: false},
			// Player6: {password: "", country: "Zumiland", playername: "Player S", abbr: "Zl2", admin: false},
			// Player7: {password: "", country: "Procyon", playername: "Player Se", abbr: "Pc2", admin: false},
			// Player8: {password: "", country: "Lotor", playername: "Player E", abbr: "Lt2", admin: false},
// }; 

exports.initialize = function (callback) //
{
	//
	// load data from database
	//
	db.loadFromFile('autobackup.txt'); //TODO: delete this, have "initialize" call another function called "refreshFromDatabase", and have a third function called "refreshFromBackup" to clear the database and load a text backup into it, then refresh
	db.get(['game'], function(err,data) {
		game.locked = false;
		game.turn = 1;
		game.turnEnds = "Never";
		setGameObject(data, function (err) {
			db.setTurn(game.turn);
			if (err) 
			{
				console.log(err);
			}
			db.get(['users'], function (err, data) { //load user data from database
				//console.log(data);
				setUsersSettings(data, function(err) { //add what was in the database to what is already in the 'users' variable, then put that back in the database
					if (err)
					{
						console.log(err);
					}
					// for (var key in usersSettings)
					// {
						// countryNames.push(usersSettings[key].country);
					// }
					// countries.initialize(countryNames);
					users.initialize(usersSettings, function(err) {
						//users.loadSampleData(); // TODO: comment this out...
						if (err)
						{
							console.log(err);
						}
						users.dbToState( function (err) {
							callback(err);
						});
					});
				});
			});
		});
	});
};

function getGameObject()
{
	return game;
}
exports.getGameObject = getGameObject;

function setGameObject(data, callback)
{
	for (var key in data)
	{
		if (key in game)
		{
			game[key] = data[key];
		}
	}
	db.setTable('game', game, callback);
}
exports.setGameObject = setGameObject;





// function getUsersSettings()
// {
	// return usersSettings;
// }
// exports.getUsersSettings = getUsersSettings;

function setUsersSettings(data, callback)
{
	for (var key in data)
	{
		usersSettings[key] = data[key];
	}
	
	db.setTable('users', usersSettings, callback);
}
//exports.setUsersSettings = setUsersSettings;


//Sean: {password: "", country: "Esperantujo", playername: "Sean J", abbr: "eo1", admin: true}
// Adds a user
function addUser(name, password, country, playername, abbreviation, admin, callback)
{
	if(arguments.length == 7)
	{
		var tmp = {};
		tmp[name] = {password: password, country: country, playername: playername, abbr: abbreviation, admin: admin};
		if (!(name in usersSettings))
		{
			setUsersSettings(tmp, function (err) {
				if (!err)
				{
					users.addUser({name: name, password: password, country: country, playername: playername, abbreviation: abbreviation, admin: admin}, function (err)
					{
						if (err)
						{
							callback(err);
						}
						else
						{
							callback(null);
						}
					});
				}
				else
				{
					callback(err);
				}
			});
		}
		else
		{
			callback(new Error("gameData.addUser, user already in database"));
		}
	}
	else
	{
		callback(new Error("gameData.addUser, improper arguments number"));
	}
}
exports.addUser = addUser;
// function removeUser(name,callback)
// {
	// db.remove('users',[name],function (err) {
		// if (!err)
		// {
			// if (name in users)
			// {
				// delete users[name];
				// users.removeUser(name)
				// callback(null);
			// }
			// else
			// {
				// callback(new Error('cannot remove user, ' + name + ' not in gameData.users'));
			// }
		// }
		// else
		// {
			// callback(err);
		// }
	// });
	
// }
// exports.removeUser = removeUser;

function getTurn()
{
	return game.turn;
}
exports.getTurn = getTurn;

// function setTurn(pTurn)
// {
	// var out = false;
	// if (!(isNaN(pTurn)))
	// {
		// game.turn = pTurn;
		// out = true;
	// }
	// return out;
// }
// exports.setTurn = setTurn;

function incrementTurn(callback)
{
	db.setTurn(game.turn + 1);
	setGameObject({turn: game.turn + 1}, callback);
}
exports.incrementTurn = incrementTurn;

function decrementTurn(callback)
{
	if (game.turn > 1) //one is kind of a magic number here, it is feasible that someone might want to choose to start a game at 0, or to go back in time for some reason
	{
		db.setTurn(game.turn - 1);
		setGameObject({turn: game.turn - 1}, callback);
	}
	else
	{
		var err = new Error("Can't decrement turn to lower than 1");
		callback(err);
	}
}
exports.decrementTurn = decrementTurn;