This is an application for playing worldsim over the internet.

It consists of server side code and client side code.

The server is node.js (JavaScript)

To run it, you first need to install node.js on the server.
Worldsim was developed for node.js v0.8.x, but may also work with newer versions.

It also requires the packages:
	socket.io 0.9.13
	mime 1.2.5
both of which are available from the npm repository

to start the server, run
node wsServer.js

then use a browser to navigate to 
http://localhost:8080/

you can logon with 
username: Admin
password: password

for more details see 
http://worldsimblog.blogspot.com/

The game is fully playable, but currently lacks some features that would make life much easier on
the administrator.  Post a comment on the blog or get in contact with me some other way, if you want
to use this code to try to run a game, and I'll help you the best I can.