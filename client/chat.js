function sendChat() //(chatArea)
{
	//console.log($('#chatInput').val());
	//sio.emit('chat',$(chatArea).val());
	
	var chatArea = '#chatInput';
	var targets = []; //don't worry about this, the server will take car of it.
//	var userTargets = [];
	//var tag = $('#chatOutputTag').val();
	var tag = 'chat';
	var newChat = new csLog(gameData.self.abbr, [], $(chatArea).val(), '0', tag, []);
	
	// $('#chatPlayersSelect > option:selected').each( function() {
		// if (!($(this).is(':hidden')))
		// {
			// userTargets.push(gameData.users[$(this).text()].abbr);
		// }
	// });

	socketOut('add',newChat);
	$(chatArea).val("");
}

function chatAdd(id, message)
{
	// $('#chatBox').val($('#chatBox').val() + "\n" + message);
	// $('#chatBox').scrollTop($('#chatBox')[0].scrollHeight - $('#chatBox').height());
	//console.log(message);
	//$('#chatBox').val($('#chatBox').val() + "\n" + message);
	
	if (id == 'err')
	{
		$('#chatBoxDiv').append($('<br>').attr('data-id',id));
		$('#chatBoxDiv').append($('<span>').text(message).attr('data-id',id));

	}
	else
	{
		var toMe = jQuery.inArray(gameData.self.abbr, state.d.log[id].userTargets);
		var attr = {'data-id': id, 'data-sender': userFromAbbreviation(state.d.log[id].sender), 'data-tag': state.d.log[id].tag, 'toMe': toMe}
		$('#chatBoxDiv').append($('<br>').attr(attr));
		$('#chatBoxDiv').append($('<span>').text(state.d.log[id].timestamp + ' ' + userFromAbbreviation(state.d.log[id].sender) + ': ' + state.d.log[id].contents).attr(attr));

	}

	if ($("#chatMainDiv").is(':hidden'))
	{
		$('#chatMinimizeButton').addClass('unreadchat');
	}
	chatRefresh(); // refresh the chat.  This is so the messages remain hidden if show_chat is selected
	//   val($('#chatBox').val() + "\n" + message);

}

function chatClear()
{
	$('#chatBox').val(""); //clear current messages
}

function initializeChat()
{
	//// button functions
	$('#chatMinimizeButton').click(function() {
		//chattypes.hide();
		//$("#chatArea > ul > li > a").removeClass("tabselected");
		//$('#chatAreaMinimize').hide();
		//$('#chatArea').css({opacity: 0.4});
		if( $("#chatMainDiv").is(':hidden'))
		{
			$("#chatMainDiv").show();
			$("#showHideChatSettings").show();
			//$("#chatSettings").show();
			$('#chatMinimizeButton').removeClass('unreadchat');
			$('#chatArea').removeClass('chatHidden');
			$('#pageInnerWrapper').removeClass('chatHidden');
		}
		else
		{
			$("#chatMainDiv").hide();
			$("#showHideChatSettings").hide();
			
			$("#chatSettings").hide();
			$('#chatMainDiv').addClass('settingsHidden');
			
			$('#chatArea').addClass('chatHidden');
			$('#pageInnerWrapper').addClass('chatHidden');
		}
	});

	$('#chatShowHideButton').click(function() {
		if ($('#chatSettings').is(':hidden'))
		{
			$('#chatSettings').show();
			$('#chatMainDiv').removeClass('settingsHidden');
		}
		else
		{
			$('#chatSettings').hide();
			$('#chatMainDiv').addClass('settingsHidden');
		}
	});
	
	$('#chatSendButton').click(function() {
		sendChat('#chatInput');
	});	
	
	$('#chatClearErrorMessagesButton').click(function() {
		$('#chatBoxDiv').children("[data-id='err']").each( function () {
			$(this).remove();
		});
	});	
	
	$('#chatInput').keypress(function(event) {
		if ( event.which == 13 ) {
			event.preventDefault();
			sendChat('#chatInput');
		}
	});
	
	$('#chatAreaMinimize').show();
	//$('#chatArea').css({opacity: 1});
	
	
	$("#chatChatsCbx").change( chatRefresh );
	$("#chatGameEventsCbx").change( chatRefresh ).change();
	
	/////// initial populate chat list
	if ('log' in state.d)
	{
		var logKeys = Object.keys(state.d.log).sort(function(a,b){return Number(a)-Number(b)});
		for (var i in logKeys)
		{
			chatAdd(logKeys[i]);
		}
	}
	
	//make room at the bottom of the main page for the chat window
	$("#spaceFillerForChat").height($("#chatArea").height());

}

function updateChat(action, type, id, field, value)
{
	if (type == 'log')
	{
		if (action == 'update')
		{
			chatAdd(id);
		}
	}
}

function chatRefresh ()
{
	var tags = {};
	var users = {};
	var includeChats = $("#chatChatsCbx").is(':checked');
	var includeGameEvents = $("#chatGameEventsCbx").is(':checked');
	
	
	// $('#chatTagsSelect > option:selected').each( function () {
		// tags[$(this).text()] = 1;
	// });
	
	// $('#chatPlayersSelect > option:selected').each( function () {
		// users[gameData.users[$(this).text()].abbr] = 1;
	// });
	
	if (includeGameEvents)
	{
		users.sys = 1;
	}
	
	$('#chatBoxDiv').children().each( function () {
		$(this).hide();

		if ($(this).attr('data-id') == 'err')
		{
			//always show error messages, they can delete them if they want
			$(this).show();
		}
				//        message is a chat and chatCbx is selected, or  message is a game event and includeGame Events cbx is selected
		else if (($(this).attr('data-tag') == 'chat' && includeChats === true) || ( ($(this).attr('data-tag').slice(0,1) == '_') && includeGameEvents)  ) //me in usertargets or includeTeammates) )
		{
			$(this).show();
		}
	});
	$('#chatBoxDiv').scrollTop($('#chatBoxDiv')[0].scrollHeight);
}

function chatPopulateTags ()
{
	$('#chatTagsSelect').empty();
	if ('log' in state.d)
	{
		var tags = {};
		for (var i in state.d.log)
		{
			tags[state.d.log[i].tag] = state.d.log[i].sender;
			//$('#chatTagsSelect').append($('<option> </option>').text(state.d.log[i].tag));
		}
		for (var i in tags)
		{	
			//console.log(i);
			//console.log(tags[i]);
			if ( $('#chatPlayersSelect > option[data-player="'+ userFromAbbreviation(tags[i]) + '"]').filter(":selected").not(':hidden').length > 0)
			{
				//console.log($('#chatPlayersSelect > option[data-player="'+ userFromAbbreviation(tags[i]) + '"]').length);
				$('#chatTagsSelect').append($('<option> </option>').text(i).attr('selected','selected'));
			}
		}
	}
}