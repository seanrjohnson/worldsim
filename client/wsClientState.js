// a class to store the state of a client

var defaultSender = 'sys'; //was wsg
function wsClientState()
{
	this.currency = 'points';  //costs measured in units of the resource with this species name.
	this.d = {};  // d for data
}

wsClientState.prototype = {

	// add or replace an entry
	update: function(type, ID, entry) {
		if (!(type in this.d))
		{
			this.d[type] = {};
		}
		this.d[type][ID]= entry;
		 //this.logIDs.push(ID);
	},
	
	//change the value of a subfield of an entry
	updateField: function(type, ID, field, value)
	{
		if ((type in this.d) && (ID in this.d[type]) && (field in this.d[type][ID]))
		{
			this.d[type][ID][field] = value;
		}
	},
	
	//remove an entry
	remove: function(type, ID) {
		if ((type in this.d) && (ID in this.d[type]))
		{
			delete this.d[type][ID];
		}
	},
	
	//given a type and a set of field criteria, find a list of IDs matching the criteria
	getIDs: function (type, criteria)
	{
		var hits = [];
		if (type in this.d)
		{
			for (var ID in this.d[type])
			{
				var hit = true;
				var critNum = Object.keys(criteria).length;
				var critMatch = 0;
				for (var criterion in criteria)
				{
					//console.log(criterion);
					//console.log(criteria);
					//console.log(this.d[type][ID][criterion]);
					if ((criterion in this.d[type][ID]) && (criteria[criterion] == this.d[type][ID][criterion]))
					{
						//console.log(this.d[type][ID][criterion]);
						critMatch++;
					}
				}
				if (critMatch < critNum)
				{
					hit = false;
				}
				if (hit)
				{
					hits.push(ID);
				}
			}
		}
		return hits;
	},
	//  Records must NOT have nested data structures as fields
	templates: { 
		log: function() { //template for the log type
			//(sender, targets, contents, time, tag, userTargets)
			return new csLog('string',['string'],'string', 'string', 'string', ['string']);
		},
		
		policy: function() { //template for the policy type
			//(sender, targets, text, name, resourcesCreated, resourcesLiquidated, resourcesSpent)
			return new csPolicy('string',['string'],'string', 'string', {string: 1}, {string: 1}, {string: 1}, true, true); //TODO: make sure the 1 in the hashes are being validated correctly in the validateRecordStructure function
		},
		//function csResource(targets, species, current, start, minimum, createCost, liquidateCost, canSell, canBuy, canLiquidate, canCreate, canSpend, regenerates, tag)
		resource: function() { 
			return new csResource(['string'],'string', 1, 1, 1, 1, 1, true, true, true, true, true, true, 'string');
		},
		military: function() { 
			return new csMilitary ('string',['string'],'string', 'string', 'string', 'string', {string: 1}, 'string');
		},
		tax: function() {
			//function csTax(sender, targets, name, text, points)
			return new csTax('string',['string'],'string','string',1);
		},
		relation: function() {
			//(targets, country1, country2, status, embassy1, embassy2, pointsIn1, pointsIn2)
			return new csRelation(['string'],'string','string','string','string','string',1,1);
		},
		treaty: function() {
			//(sender, targets, name, text, status)
			return new csTreaty('string',['string'],'string','string',['string']);
		},
		trade: function() {
			//(sender, targets, recipients, outgoing, incoming)
			return new csTrade('string',['string'],['string'],{'string':1},{'string':1},true,1);
		},
		cover:function() { //(targets, data)
			return new csCover(['string'],['string']);
		},
		map:function() { // (targets, url, text)
			return new csMap(['string'],'string','string');
		},
		mail:function() { // (sender, targets, countrySender, countryTargets, name, text, userSender, readBy)
			return new csMail('string',['string'],'string',['string'],'string','string', 'string', ['string']);
		}
	},
	log: {
		maxTagLength: 10
	},
	policy: {
		check: function(pPolicy, obj) { //calculate the cost of a policy, and whether or not it is possible
			var totalCost = 0;
			var possible = true;
			//console.log(obj);
			var currency = obj.currency;
			var currencyRecords = obj.getIDs('resource', {species: currency});
			var netPointsChange = 0;
			//debugger;
			for (var key in pPolicy.resourcesCreated)
			{
				var resourceRecords = obj.getIDs('resource', {species: key});//should be have length of exactly 1, but if it's > 1, obj isn't the place to catch it
				if (resourceRecords.length > 0 && pPolicy.resourcesCreated[key] >= 0) 
				{
					var firstRecord = obj.d.resource[resourceRecords[0]];
					totalCost += (firstRecord.createCost * pPolicy.resourcesCreated[key]);
					if (!firstRecord.canCreate)
					{
						//console.log('cant create');
						possible = false;
					}
				}
				else
				{
					possible = false;
				}
			}
			for (var key in pPolicy.resourcesLiquidated)
			{
				var resourceRecords = obj.getIDs('resource', {species: key});//should be have length of exactly 1, but if it's > 1, obj isn't the place to catch it
				if (resourceRecords.length > 0 && pPolicy.resourcesLiquidated[key] >= 0) 
				{
					var firstRecord = obj.d.resource[resourceRecords[0]];
					var totalLoss = pPolicy.resourcesLiquidated[key]; //total amount of the resource lost in this policy
					if (key in pPolicy.resourcesSpent)
					{
						totalLoss += pPolicy.resourcesSpent[key];
					}
					
					totalCost -= (firstRecord.liquidateCost * pPolicy.resourcesLiquidated[key]);
					if (!(firstRecord.canLiquidate && (totalLoss <= firstRecord.current)))
					{
						possible = false;
					}
				}
				else
				{
					possible = false;
				}
			}
			for (var key in pPolicy.resourcesSpent)
			{
				var resourceRecords = obj.getIDs('resource', {species: key});//should be have length of exactly 1, but if it's > 1, obj isn't the place to catch it
				if (resourceRecords.length > 0 && pPolicy.resourcesSpent[key] >= 0) 
				{
					var firstRecord = obj.d.resource[resourceRecords[0]];
					var totalLoss = pPolicy.resourcesSpent[key];
					if (key == currency)
					{
						//totalCost += pPolicy.resourcesSpent[key];
						//totalCost += 0;
					}
					
					if (key in pPolicy.resourcesLiquidated)
					{
						totalLoss += pPolicy.resourcesLiquidated[key];
					}
					if (!(firstRecord.canSpend) || totalLoss > firstRecord.current)
					{
						possible = false;
					}
				}
				else
				{
					possible = false;
				}
			}
			
			if (currencyRecords >= 0)
			{
				var firstRecord = obj.d.resource[currencyRecords[0]];
				netPointsChange = (-1) * totalCost;
				if (currency in pPolicy.resourcesCreated)
				{
					netPointsChange += pPolicy.resourcesCreated[currency];
				}
				if (currency in pPolicy.resourcesLiquidated)
				{
					netPointsChange -= pPolicy.resourcesLiquidated[currency];
				}
				if (currency in pPolicy.resourcesSpent)
				{
					netPointsChange -= pPolicy.resourcesSpent[currency];
				}
				if (firstRecord.current < (-1)*netPointsChange)
				{
					possible = false;
				}
			}
			else
			{
				possible = false;
				console.log("no currency defined");
			}
			return {cost: netPointsChange, possible: possible};
		},
		checkUndo: function (pPolicy, obj) {
			// var totalCost = 0;
			// var possible = true;
			// //console.log(obj);
			// var currency = obj.currency;
			// var currencyRecords = obj.getIDs('resource', {species: currency});
			// var netPointsChange = 0;
			// var netChange = {}; //key = resource name, net change in the resource caused by the policy
			// //debugger;
			// for (var key in pPolicy.resourcesCreated)
			// {
				// var resourceRecords = obj.getIDs('resource', {species: key});//should be have length of exactly 1, but if it's > 1, obj isn't the place to catch it
				// if (!(key in netChange))
				// {
					// netChange{key} = 0;
				// }
				// if (resourceRecords.length > 0 && pPolicy.resourcesCreated[key] >= 0) 
				// {
					// var firstRecord = obj.d.resource[resourceRecords[0]];
					// totalCost += (firstRecord.createCost * pPolicy.resourcesCreated[key]);
					// netChange{key} += pPolicy.resourcesCreated;
				// }

			// }
			// for (var key in pPolicy.resourcesLiquidated)
			// {
				// var resourceRecords = obj.getIDs('resource', {species: key});//should be have length of exactly 1, but if it's > 1, obj isn't the place to catch it
				// if (resourceRecords.length > 0 && pPolicy.resourcesLiquidated[key] >= 0) 
				// {
					// var firstRecord = obj.d.resource[resourceRecords[0]];
					// var totalLoss = pPolicy.resourcesLiquidated[key]; //total amount of the resource lost in this policy
					// if (key in pPolicy.resourcesSpent)
					// {
						// totalLoss += pPolicy.resourcesSpent[key];
					// }
					
					// totalCost -= (firstRecord.liquidateCost * pPolicy.resourcesLiquidated[key]);
					// if (!(firstRecord.canLiquidate && (totalLoss <= firstRecord.current)))
					// {
						// possible = false;
					// }
				// }
				// else
				// {
					// possible = false;
				// }
			// }
			// for (var key in pPolicy.resourcesSpent)
			// {
				// var resourceRecords = obj.getIDs('resource', {species: key});//should be have length of exactly 1, but if it's > 1, obj isn't the place to catch it
				// if (resourceRecords.length > 0 && pPolicy.resourcesSpent[key] >= 0) 
				// {
					// var firstRecord = obj.d.resource[resourceRecords[0]];
					// var totalLoss = pPolicy.resourcesSpent[key];
					// if (key == currency)
					// {
						// //totalCost += pPolicy.resourcesSpent[key];
						// //totalCost += 0;
					// }
					
					// if (key in pPolicy.resourcesLiquidated)
					// {
						// totalLoss += pPolicy.resourcesLiquidated[key];
					// }
					// if (!(firstRecord.canSpend) || totalLoss > firstRecord.current)
					// {
						// possible = false;
					// }
				// }
				// else
				// {
					// possible = false;
				// }
			// }
			
			// if (currencyRecords >= 0)
			// {
				// var firstRecord = obj.d.resource[currencyRecords[0]];
				// netPointsChange = (-1) * totalCost;
				// if (currency in pPolicy.resourcesCreated)
				// {
					// netPointsChange += pPolicy.resourcesCreated[currency];
				// }
				// if (currency in pPolicy.resourcesLiquidated)
				// {
					// netPointsChange -= pPolicy.resourcesLiquidated[currency];
				// }
				// if (currency in pPolicy.resourcesSpent)
				// {
					// netPointsChange -= pPolicy.resourcesSpent[currency];
				// }
				// if (firstRecord.current < (-1)*netPointsChange)
				// {
					// possible = false;
				// }
			// }
			// else
			// {
				// possible = false;
				// console.log("no currency defined");
			// }
			// return possible;
		}	
	},
	resource: {
		idFromSpecies: function (species, obj) {
			var out = null;
			if ('resource' in obj.d)
			{
				for( id in obj.d.resource)
				{
					if (obj.d.resource[id].species == species)
					{
						out = id;
						break;
					}
				}
			}
			return out;
		},
		getFirstID: function (species, obj) //obj is the client state object
		{
			var out = {};
			out.err = false;
			out.value = -1;
			
			var listOfResources = obj.getIDs('resource', {species: species});
			if (listOfResources.length > 0)
			{
				out.value = listOfResources[0];
			}
			else
			{
				out.err = true;
			}
			
			return out;
		}
		
	},
	relation: {
		getCountryPos: function (name, rec) //rec is the relation record
		{
			var position = 0;
			//we want to find out which of the countries in the record is the one named in 'name', 0 if it is neither
			//console.log(id);
			if (rec.country1 == name)
			{
				position = 1; 
			}
			else if (rec.country2 == name)
			{
				position = 2;
			}
			return position;
		},
		// tradePartners: function (country1, country2, relations) //country1 and country2 are the names of (different) countries, relations is an object of csRelation objects (presumably all of those in the state object) where the key is arbitrary
		// {
			// var out = false;
			// if (country1 == country2)
			// {
				// out = true;
			// }
			// for (var i in relations)
			// {
				// if ((relations[i].country1 == country1 && relations[i].country2 == country2) || (relations[i].country2 == country1 && relations[i].country1 == country2))
				// {
					
					// if (relations[i].embassy1 == "E" && relations[i].embassy2 == "E")
					// {
						// out = true;
					// }
				// }
			// }
			// return out;
		// },
		findRelation: function (country1, country2, state)
		{
			var out = {};
			out.err = false;
			
			for (var rec in state.d.relation)
			{
				if ( (country1 == state.d.relation[rec].country1 && country2 == state.d.relation[rec].country2) || (country1 == state.d.relation[rec].country2 && country2 == state.d.relation[rec].country1) )
				{
					out.rec = state.d.relation[rec];
				}
			}
			
			if (!('rec' in out))
			{
				err = true;
			}
			return out;
		},
		tradePartners: function (country1, country2, state)
		{
			var out = false;
			var rec = state.relation.findRelation(country1,country2,state);
			
			if (rec.err == false)
			{
				if (rec.rec.embassy1 == 'E' && rec.rec.embassy2 == 'E')
				{
					out = true;
				}
			}
			return out;
		},
		costs: {good:3, shaky:5, bad:9}
	},
	treaty: {
		// returns -1 if country is not among the targets of the treaty, else returns the country index (which is the same for both rec.targets and rec.status)
		getCountryIndex: function(name, rec) //name is country name, rec is the treaty record
		{
			var out = -1;
			for (var i = 0; i < rec.targets.length; i++)
			{
				if (rec.targets[i] == name)
				{
					out = i;
				}
			}
			return out;
		},
		//check that only valid countries are targets, that there is only one of each target, that there are the same number of targets as statuses, that the sender is among the targets, that the sender's status is 'signed'
		check: function(pTreaty, pCountries, pSender)
		{
			var out = true; // set out to true, and then change it to false if any of the necessary contitions are not met
			
			var countriesHash = {};
			
			var targets = 0;
			var statuses = 0;
			var senderIndex = -1;
			//var acceptableStatuses = {signed: 1, pending: 1, withdrawn: 1, declined: 1}
			
			for (var i = 0; i < pTreaty.targets.length; i++)
			{
				targets += 1;
				if (pTreaty.targets[i] in countriesHash)
				{
					out = false; //it's a repeat
				}
				else
				{
					countriesHash[pTreaty.targets[i]] = 1;
					if (!(pTreaty.targets[i] in pCountries))
					{
						out = false;
					}
				}
				if (pTreaty.targets[i] == pSender)
				{
					senderIndex = i;
				}
			}
			
			if (senderIndex == -1)
			{
				out = false; //sender not in targets
			}
			else
			{
				var statuses = 0;
				for (var i = 0; i < pTreaty.status.length; i++)
				{			
					statuses += 1;
					if (i == senderIndex && pTreaty.status[i] != 'signed')
					{
						out = false;
					}
					if (i != senderIndex && pTreaty.status[i] != 'pending')
					{
						out = false;
					}
				}
				if (statuses != targets) //same number of targets as statuses
				{
					out = false;
				}
			}
			return out;
		
		},
		checkModify: function(pTreaty, newStatus, pSender)
		{
			var out = true;
			var legalTransitions = {withdrawn: 'signed', signed: 'pending', declined: 'pending'}; //key is requested new state, value is state currently in the record.
			
			
			if (newStatus.length != pTreaty.status.length)
			{
				//the lengths of the new status array is different from the old one
				console.log('badLength');
				out = false;
			}
			else
			{
				for (var i = 0; i < pTreaty.status.length; i++)
				{
					if (pTreaty.targets[i] == pSender)
					{
						if(!((newStatus[i] in legalTransitions) && (legalTransitions[newStatus[i]] == pTreaty.status[i]) ) )
						{
							// the change in the senders status is not a legal transition
							console.log('illegalTransition');
							out = false;
						}
					}
					else
					{
						if (newStatus[i] != pTreaty.status[i])
						{
							console.log('ChangeToForeignStatus');
							// the request also changes some-one elses status
							out = false;
						}
					}
				}
			}	
			return out;
		},
		getPosition: function(pTreaty, pCountry)
		{
			var out = -1;
			for (var i = 0; i < pTreaty.status.length; i++)
			{
				if (pTreaty.targets[i] == pCountry)
				{
					out = i;
				}
			}
			return out;
		}
		
	},
	trade: {
		check: function (record, countries, state, senderCountry, invert)
		{
			// check that all incoming and outgoing values are positive.  check that the sender can buy the objects in the incoming list, and can sell the objects in the outgoing list. check that all countries in targets are valid countries.
			// also checks that tradesLeft is > 0
			// if invert = true, assume the state object passed if for a perspective accepter of the treaty and so he must be able to buy things in the outgoing list and so forth.
			var valid = true
			var incoming = 'incoming';
			var outgoing = 'outgoing';
			
			if (invert === undefined)
			{
				invert = false;
			}
			
			
			var canBuyList = hashify(state.getIDs('resource', {canBuy: true}, 1)); //returns an object with keys as all the resources the player can buy
			var canSellList = hashify(state.getIDs('resource', {canSell: true}, 1)); //returns an object with keys as all the resources the player can sell
			
			if (invert)
			{
				incoming = 'outgoing';
				outgoing = 'incoming';
			}

			for (var i in record[incoming])
			{
				var id = state.resource.getFirstID(i, state);
				if ((record[incoming][i] <= 0) || (id.err) || !(id.value in canBuyList))
				{
					console.log('q1');
					valid = false;
				}
			}
			
			for (var i in record[outgoing])
			{
				var id = state.resource.getFirstID(i, state);
				if ((record[outgoing][i] <= 0) || (id.err) || !(id.value in canSellList))
				{
					console.log('q2');
					valid = false;
				}
			}
			
			//don't allow empty trades to be posted
			if (Object.keys(record[outgoing]).length + Object.keys(record[incoming]).length == 0)
			{
				console.log('q3');
				valid = false;
			}
			
			//all recipients are countries
			for (var i in record.recipients)
			{
				if (!(record.recipients[i] in countries))
				{
					console.log('q4');
					valid = false;
				}
			}
			
			//the first country in targets must be the sender country, the rest must be the recipient countries
			if ( (senderCountry != record.targets[0]) || (record.targets.length - 1 != record.recipients.length) || (Object.keys(hashify(record.recipients)).length != record.recipients.length))
			{
				//sender is not the first target OR there are more recipients than targets - 1 OR there are repeats in recipients
				console.log('q5');
				valid = false;
			}
			else
			{
				for (var i = 1; i < record.targets.length; i++)
				{
					if (!(record.targets[i] == record.recipients[i-1]))
					{
						console.log('q6');
						valid = false;
					}
				}
			}
			
			//tradesLeft must be 1 or greater
			if (!(record.tradesLeft >= 1))
			{
				valid = false;
			}
			

			return valid;
		},
		checkAccept: function (record, senderState, accepterState)
		{
			// checks to see that the sender and accepter have enough resources to cover the trade (does not check if the resources are tradeable, that should have already been checked)
			var valid = true;
			var buyCost = 0;
			
			
			var netPointsIn = 0;
			if (senderState.currency in record.outgoing)
			{
				netPointsIn -= record.outgoing[senderState.currency];
			}
			
			if (accepterState.currency in record.incoming)
			{
				netPointsIn += record.incoming[accepterState.currency];
			}
			
			for (var i in record.outgoing)
			{
				var id = senderState.resource.getFirstID(i, senderState);
				// console.log(senderState.d.resource[id.value].current);
				// console.log(senderState.d.resource[id.value].current);
				if (id.err == true)
				{
					console.log('y1');
					valid = false;
				}
				else if ((senderState.d.resource[id.value].current < record.outgoing[i]) && senderState.d.resource[id.value].canCreate)
				{
					buyCost += (record.outgoing[i] - senderState.d.resource[id.value].current) * senderState.d.resource[id.value].createCost;
				}
				else if (senderState.d.resource[id.value].current < record.outgoing[i])
				{
					console.log('y2');
					valid = false;
				}
			}
			
			for (var i in record.incoming)
			{
				var id = accepterState.resource.getFirstID(i, accepterState);
				if ((id.err == true) ||(accepterState.d.resource[id.value].current < record.incoming[i]))
				{
					console.log('y3');
					valid = false;
				}
			}
			
			//resources must be bought, and buy resources to meet demand = true, and the sender must have enough money to buy the resources
			if ((buyCost > 0) && (record.buyToMeetDemand == true))
			{
				if (buyCost <= netPointsIn + senderState.d.resource[senderState.resource.getFirstID(senderState.currency, senderState).value].current)
				{
				
				}
				else
				{
					console.log('y3');
					valid = false;
				}
			}
			else if (buyCost > 0)
			{
				console.log('y4');
				valid = false;
			}
			//console.log(valid);
			return valid;
		},
		buyAmounts: function (record, senderState)
		{
			var out = {};
			for (var i in record.outgoing)
			{
				var id = senderState.resource.getFirstID(i, senderState);
				// console.log(senderState.d.resource[id.value].current);
				// console.log(senderState.d.resource[id.value].current);
				if (id.err == true)
				{
				
				}
				else if (senderState.d.resource[id.value].current < record.outgoing[i])
				{
					out[i] = (record.outgoing[i] - senderState.d.resource[id.value].current);
				}
			}
			return out;
		},
		buyCost: function (record, senderState)
		{
			var out = 0;
			for (var i in record.outgoing)
			{
				var id = senderState.resource.getFirstID(i, senderState);
				// console.log(senderState.d.resource[id.value].current);
				// console.log(senderState.d.resource[id.value].current);
				if (id.err == true)
				{

				}
				else if (senderState.d.resource[id.value].current < record.outgoing[i])
				{
					out += (record.outgoing[i] - senderState.d.resource[id.value].current) * senderState.d.resource[id.value].createCost;
				}
			}
			return out;
		}
	},
	mail: {
		//check that only valid countries are targets, that there is only one of each target
		check: function(pMail, pCountries, pSender)
		{
			var out = true; // set out to true, and then change it to false if any of the necessary contitions are not met
			
			var countriesHash = {};
			
			var targets = 0;
			
			for (var i = 0; i < pMail.countryTargets.length; i++)
			{
				targets += 1;
				if (pMail.countryTargets[i] in countriesHash)
				{
					out = false; //it's a repeat
				}
				else
				{
					countriesHash[pMail.countryTargets[i]] = 1;
					if (!(pMail.countryTargets[i] in pCountries))
					{	//country is not a real country
						out = false;
					}
				}
			}
			return out;
		}		
	}
};


//a class for log entries
function csLog(sender, targets, contents, time, tag, userTargets)
{
	//mandatory in all state.data classes
	// sender is a string (should be a user abbreviation)
	//
	this.sender = sender; //could be 'sys' or something like that if it is not associated with any country in particular (see defaultSender above)
	this.targets = targets; // an array of country names
	this.type = 'log';
	
	//specific to log data
	this.timestamp = time;
	this.tag = tag;
	this.contents = contents;
	this.userTargets = userTargets;
	
}

function csPolicy (sender, targets, text, name, resourcesCreated, resourcesLiquidated, resourcesSpent, enacted)
{
	this.sender = sender;
	this.targets = targets;
	this.type = 'policy';
	
	this.text = text;
	this.name = name;
	this.resourcesCreated = resourcesCreated;   // object with species names as keys and amount as values
	this.resourcesLiquidated = resourcesLiquidated; // object with species names as keys and amount as values
	this.resourcesSpent = resourcesSpent; // object with species names as keys and amount as values
	this.enacted = enacted;
	this.removalRequested = false; //you'd never make a new policy with removal requested (I don't think)
}

function csMilitary (sender, targets, name, destination, publicReason, realReason, unitsAllocated, status)
{
	this.sender = sender;
	this.targets = targets;
	this.type = 'military';
	
	this.name = name; //string
	this.destination = destination;  //string
	this.publicReason = publicReason; //string
	this.realReason = realReason; //string
	this.unitsAllocated = unitsAllocated; // object with resource species names as keys and amount as values, (species should be ones where tag = military)
	
	// The player decides on a move, and creates the record with status = 'deploying'
	// While status = 'deploying' the player is free to cancel the orde (delete the record)
	// The admin will see moves in the 'deploying' queue and change them to status = 'active'.
	// once a move is 'active' the player can change it betwee status = 'withdrawing' and status = 'active'.
	// When the admin sees a 'withdrawing' military action, he can delete it.
	if (status == null)
	{
		this.status = 'deploying'; //string, one of: 'deploying', 'active', 'withdrawing'
	}
	else
	{
		this.status = status;
	}
}

function csResource(targets, species, current, start, minimum, createCost, liquidateCost, canSell, canBuy, canLiquidate, canCreate, canSpend, regenerates, tag)
{
	this.type = 'resource'; 
	this.sender = defaultSender;
	this.targets = targets;
	
	this.species = species // the particular kind of resource
	
	this.current = current; //current inventory
	
	this.canSell = canSell; //sell or buy from other countries
	this.canBuy = canBuy;
	
	this.canLiquidate = canLiquidate; //sell or buy for point cost within ones own country
	this.canCreate = canCreate;
	this.createCost = createCost;
	this.liquidateCost = liquidateCost;
	
	this.canSpend = canSpend;
	this.regenerates = regenerates; //if it comes back next turn after it is spent
	
	this.start = start; // inventory at start of turn
	this.minimum = minimum; //minimum inventory at end of turn to avoid penalties
	if (!(tag == null))
	{
		this.tag = tag;
	}
	else
	{
		this.tag = "";
	}
}


function csTrade (sender, targets, recipients, outgoing, incoming, buyToMeetDemand, tradesLeft)
{
	this.sender = sender;
	this.targets = targets; //all countries that should have this record in their state object, so the union of sender->country, and recipients
	// in sender->country must be the first country in the list, with recipients following.
	
	this.recipients = recipients;
	this.type = 'trade';
	
	
	this.outgoing = outgoing; //object.  keys are resource names. values are number of that resource that will be transferred from the sender country to the accepting target country.
	this.incoming = incoming; //object.  keys are resource names. values are number of that resource that will be transferred from the accepting target country to the sender country.
	this.buyToMeetDemand = buyToMeetDemand;
	this.tradesLeft = tradesLeft;
}


function csTax(sender, targets, name, text, points)
{
	this.sender = sender;
	this.targets = targets;
	this.type = 'tax';
	
	this.name = name;
	this.text = text;
	this.points = points;
}

//
// These objects are shared between two countries
// 
//
function csRelation (targets, country1, country2, status, embassy1, embassy2, unitsIn1, unitsIn2)
{
	this.sender = defaultSender; //doesn't belong to anyone defaultSender
	this.targets = targets; //the countries that share the relation
	this.type = 'relation'; 
	
	this.country1 = country1; //should be the same countries as in "targets" but they need to be ordered, see below
	this.country2 = country2;
	
	this.status = status; // good, shaky or bad
	
	
	this.embassy1 = embassy1; // 'E' if country1 has an embassy in country2, 'R' if country1 requests an embassy in country2, 'N' if country1 has no embassy in country2 and is not requesting one 
	this.embassy2 = embassy2;
	
	this.unitsIn1 = unitsIn1; //number of diplomacy points country1 has invested in diplomatic infrastructure in country2 (the physical presence in that country as well as supporting operations in the home country)
	this.unitsIn2 = unitsIn2;
}

function csTreaty (sender, targets, name, text, status)
{
	this.sender = sender;
	this.targets = targets;
	this.type = 'treaty';
	
	this.name = name; //string
	this.text = text; //string
	this.status = status; //an array of strings.  Each index 'i' corresponds to the country listed as targets[i].  Values are can be either 'signed', 'pending', 'withdrawn', 'declined'.
}

// contains general information about the country for that turn
function csCover(targets, data) { //data is an array with each entry being a piece of data about the country
	this.sender = defaultSender; //doesn't originate with any country, so use the default sender
	this.targets = targets;
	this.type = 'cover';
	
	this.data = data; //data is an array where each entry is a text line in the cover
}

// contains general information about the country for that turn
function csMap(targets, url, text) { //data is an array with each entry being a piece of data about the country
	this.sender = defaultSender; //doesn't originate with any country, so use the default sender
	this.targets = targets; //should be the same for all countries
	this.type = 'map';
	
	this.url = url;
	this.text = text;
}

function csMail(sender, targets, countrySender, countryTargets, name, text, userSender, readBy)
{
	//set default parameter for userSender and readBy
	userSender = typeof userSender !== 'undefined' ? userSender : '';
	readBy = typeof readBy !== 'undefined' ? readBy : [];
	
	this.sender = sender;
	this.targets = targets;
	this.type = 'mail';
	
	this.countrySender = countrySender;
	this.countryTargets = countryTargets;
	this.userSender = userSender;
	this.name = name; //string
	this.text = text; //string
	this.readBy = readBy; //list of usernames of people who have read it.
}

function validateRecordStructure (record)
{
	var out = false //if the type is invalid, return false
	if('type' in record && (record.type in wsClientState.prototype.templates))
	{
		//console.log('4');
		out = true; // if the type is valid, assume true, then look for contradictions
		var template = wsClientState.prototype.templates[record.type]()
		//console.log(record);
		//console.log(template);
		for (var key in record)
		{
			//console.log(key);
			//console.log(key in template);
			//console.log(validateField(record[key],template[key]));
			if (!(key in template && validateField(record[key],template[key])))
			{
				console.log("field " + key );
				out = false;
			}
		}
	}
	return out;
}

// record is the value contained in a record field, template is the value contained in the same field of the template object
function validateField(record, template)
{
	var out = true; //assume true, then search for contradictions
	var recordType = getVarType(record);
	var templateType = getVarType(template);
	
	// console.log(record);
	// console.log(template);
	
	if (templateType == 'number') //JSON converts numbers into strings...
	{
		if (isNaN(parseInt(record)))
		{
			out = false;
		}
	}
	else if (!(recordType == templateType))
	{
		//console.log(recordType + " " + templateType);
		out = false;
	}
	else if (recordType == 'array')
	{
		var templateSubType = getVarType(template[0]);
		for (var i = 0; i < record.length; i++) // note, returns true on empty arrays
		{
			if (getVarType(record[i]) != templateSubType)
			{
				//console.log(getVarType(record[i]) + " " + templateSubType);
				out = false;
			}
		}
	}
	else if (recordType == 'object')
	{
		recKeys = Object.keys(record);
		templateKeys = Object.keys(template);
		var templateSubType = getVarType(template[templateKeys[0]]);
		for (var i = 0; i < recKeys.length; i++) // note, returns true on empty objects
		{
			if (getVarType(record[recKeys[i]]) != templateSubType)
			{
				//console.log(getVarType(record[recKeys[i]]) + " " + templateSubType);
				out = false;
			}
		}
		
	}
	return out;
}


// stolen from http://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator/

function getVarType(obj) {
	  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();  //would be good to get a regex free version of this
}


/*
*
* turn an array into a hash with keys as the array members and values as val
*
*/
function hashify(arr,val)
{
	var out = {};
	for (var i = 0; i < arr.length; i++)
	{
		out[arr[i]] = val;
	}
	return out;
}
