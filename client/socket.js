//
// holds socket functions
//
function socketIn(sio)
{

	sio.on('initialization', function(data) {
		replaceData(data);
		initializeLayout();
		
		//hide all of the views
		$('#main > div').each( function(){
			$(this).hide();
		});
		$('#rightArea > div > div').each( function(){
			$(this).hide();
		});
		$('#leftArea > div').each( function(){
			$(this).hide();
		});
		
		
		initializeViews();
		initializeChat();

		displayView('viewoffers',true);
		displayView('resources',true);
		displayView('mail',true);
		displayView('users',true);
		//refreshChat();
		$("body").show();
	});
	
	sio.on('newTurn', function(data) { //if a user happens to be logged on during a turn transition, this is called
		//TODO: Stop sending 'data' because it will send after the reload anyways
		//replaceData(data);
		//initializeViews();
		
		//Reloading the page should be all that needs to happen.
		location.reload();
	});
	
	
	function replaceData(data) //replace 'state' and 'gameData' with dumps from the server
	{
		state.d = data.state;
		gameData = data.gameData;
	}
	// //Entire state object was transmitted, so use data to update state object
	// sio.on('stateDump', function(data) {
		// state = data;
		// //maybe should also refresh current view, and/or chat
	// });

	// sio.on('userDump', function(data) {
		// userData = data;
		// //probably some other things need to be updated at this time too.
	// });

	// update game Data
	sio.on('gameUpdate', function(data) {
		//update the variable
		gameData = mergeObjects(data,gameData);
		
		// update the information at the top of the screen
		if (gameData.game.locked == true)
		{
			$('#topspaceLocked').text('GAME LOCKED');
		}
		else
		{
			$('#topspaceLocked').text('');
		}
		
		$('#topspaceTurn').text(String(gameData.game.turn));
		$('#topspaceTurnEnds').text(gameData.game.turnEnds);
		
		//tell the views to update also
		updateViews('gameUpdate');
	});
	
	sio.on('updateState', function(data) {
		//alert(data);
		state.update(data.record.type, data.id, data.record); 

		updateViews('update', data.record.type, data.id);
		updateChat('update', data.record.type, data.id);
	});
	sio.on('updateField', function(data) {
		state.updateField(data.type, data.id, data.field, data.value)
		updateViews('updateField', data.type, data.id, data.field, data.value);
	});
	sio.on('removeState', function(data) {
		//alert(data);
		state.remove(data.type, data.id); 
		updateViews('remove', data.type, data.id);
	});
	sio.on('downloadStateData', function(data) {
		//console.log(data);
		updateViews('downloadStateData','','','',data);
	});
	sio.on('reqError', function(data) {
		errors = errors.concat(data);
		for (var i = 0; i < data.length; i++)
		{
			chatAdd('err', data[i]);
		}
	});
}


function socketOut(type, data)
{
	sio.emit(type,data);
}
//socketOut('chat',{text: $(chatArea).val(), targets: targets, tags: tags});

// recursively merge properties from "from" into "into"
// base cases are anything that isn't an object, so it will iterate through associative arrays of associative arrays, but anything else will be
// treated as an end point and assigned to "into" thus if you have an associative array of arrays of associative arrays, the arrays will be copied, 
// and the associative arrays that follow will not be.
// 
//
function mergeObjects(from, into)
{
	var fromType = getVarType(from);
	var intoType = getVarType(into);
	//console.log(fromType);
	//console.log(from);
	if (fromType == "object")
	{
		if(intoType != "object")
		{
			into = {};
		}
		
		for (var key in from)
		{
			if (key in into)
			{
				into[key] = mergeObjects(from[key],into[key]);
			}
			else
			{
				into[key] = {};
				into[key] = mergeObjects(from[key],into[key]);
			}
		}
	}
	else
	{
		into = from;
	}
	//console.log(into);
	return into;
}

