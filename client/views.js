var views = {

	cover:
	{
		initialize: function() {
			if ('cover' in state.d)
			{
				$('#cover').empty();
				if ('cover' in state.d)
				{
					var data = state.d.cover[Object.keys(state.d.cover)[0]].data;
					//console.log(Object.keys(state.d.cover));
					for (var i in data)
					{
						
						$('#cover').append($('<p></p>').text(data[i]));
					}
				}
			}
		},
		update: function(action, type, id, field) {
			if (type=='cover')
			{
				views.cover.initialize();
			}
		}
	},
	
	users:
	{
		initialize: function(){
			views.users.update('gameUpdate');
		},
		update: function(action, type, id, field) {
			if (action == 'gameUpdate')
			{
				$('#usersTableBody').empty();
				keys = Object.keys(gameData.users);
				
				for (var key in gameData.users)
				{
					addUserRow(key);
				}
			}
			function addUserRow(k)
			{
				//{username: {'playername': ... , 'country': ..., 'abbr': ..., 'admin': ..., 'online': ...}}
				var o = $('<tr> </tr>');
				o.append($('<td> </td>').text(k));
				//o.append($('<td> </td>').text(gameData.users[k].abbr));
				o.append($('<td> </td>').text(gameData.users[k].playername));
				if (gameData.users[k].admin)
				{
					o.append($('<td> </td>').text(""));
					o.append($('<td> </td>').text("X"));
				}
				else
				{
					o.append($('<td> </td>').text(gameData.users[k].country));
					o.append($('<td> </td>').text(""));
				}
				if (gameData.users[k].online)
				{
					o.append($('<td> </td>').text("X"));
				}
				else
				{
					o.append($('<td> </td>').text(""));
				}
				
				
				//text(k + " " + gameData.users[k].online);
				$('#usersTableBody').append(o);
			}
		}
		
	},

	map:
	{
		initialize: function() {
			if ('map' in state.d)
			{
				var maps = Object.keys(state.d.map);
				if (maps.length > 1)
				{
					console.log('warning, more than one maps record');
				}
				map = state.d.map[maps[0]];
				$('#mapImage').attr('src',map.url);
				$('#mapWorldNews').text(map.text);
			}
			else
			{
				console.log('warning: no map to load');
			}
			$("#closeMap").hide();
			$("#closeMap").click(function(){
				$(".zoom").removeClass("zoom");
				$("#closeMap").hide();
			});
			$("#zoommap").click(function(){
				$("#map").addClass("zoom");
				$("#mapcontainer").addClass("zoom");
				$("#closeMap").show();
			});
		},
		update: function(action, type, id, field, value) {
			if(type == 'map')
			{ // whatever the action is, we want to take the url and the text of the map record
					$('#mapImage').attr('src',state.d.map[id].url);
					$('#mapWorldNews').val(state.d.map[id].text);
			}
		}
	},

	policies:
	{
		initialize: function(){
			//Policy Enacted Buttons
			$("#policyEnactedViewButton").click(function() {
				policyViewButton('Enacted');
			});
			$('#policyEnactedRemovalButton').click(function() {
				policyRequestRemovalButton();
			});

			//Saved Policy Buttons
			$('#policyViewButton').click(policyViewButton);
			$('#policyDeleteButton').click(policyDeleteButton);
			
			//New Policy Buttons
			$('#policyDeleteActionsButton').click(policyDeleteActionButton);
			$('#policyActionSelect').change(views.policies.populateAllocateKind);
			$('#policyAddActionButton').click(policyAddActionButton);
			$('#policySavePolicyButton').click(function(){policySubmit(false);});
			$('#policyResetButton').click(policyResetButton);
			$('#policyEnactPolicyButton').click(function(){policySubmit(true);});
			
			views.policies.updateInitial();

			//////// Button code ////////
			function policyRequestRemovalButton()
			{//data.type, data.id, data.field, data.value
				var selected = [];
				$("#policyEnactedSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'policy', field: 'removalRequested', value: true};
					socketOut('modify', req);
				}
			}
			
			function policyDeleteButton() {
				var selected = [];
				$("#policySelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'policy', owner: gameData.self.abbr};
					socketOut('remove', req);
				}
				
			}
			
			function policyViewButton(whatList) { //sets the right hand information to the top selected policy
				var selected = [];
				var boxTag ="";
				if (whatList == 'Enacted')
				{
					boxTag = "#policyEnactedSelect";
				}
				else
				{
					boxTag = "#policySelect";
				}
				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.policies.resetPolicy();
					var pol = state.d.policy[selected[0]];
					$('#policyName').val(pol.name);
					$('#policyText').val(pol.text);
					for (key in pol.resourcesCreated)
					{
						views.policies.addAction('Buy',state.resource.idFromSpecies(key,state),pol.resourcesCreated[key]);
					}
					for (key in pol.resourcesLiquidated)
					{
						views.policies.addAction('Liquidate',state.resource.idFromSpecies(key,state),pol.resourcesLiquidated[key]);
					}
					for (key in pol.resourcesSpent)
					{
						views.policies.addAction('Allocate',state.resource.idFromSpecies(key,state),pol.resourcesSpent[key]);
					}
				}
				views.policies.updatePointCost();
			}
			
			function policySubmit(enact) {
				///// TODO: add more validation before sending in this function
				var pol = views.policies.recordFromInput();
				
				pol.enacted = enact;
				
				//check = state.policy.check(pol,state);
				socketOut('add',pol);
			}
			
			function policyDeleteActionButton() {
				var selected = [];
				$("#policyActions option:selected").each(function () {
					$(this).remove();
				});
				views.policies.updatePointCost();
			}
			
			function policyAddActionButton() {
				if(!isNaN(parseInt($('#policyUnitNumber').val())))
				{
					var selected = [];
					$("#policyAllocateKind option:selected").each(function () {
						selected.push($(this).attr('data-recordID'));
					});
					for (var i = 0; i < selected.length; i++)
					{
						views.policies.addAction($('#policyActionSelect').val(),selected[i], parseInt($('#policyUnitNumber').val()));
					}
				}
				views.policies.updatePointCost();
			}
			
			function policyResetButton() {
				views.policies.resetPolicy();
			}
			
			
			
			//$('#policies').show();
		},
		updateInitial: function() {
			///// Policy Lists Update
			$("#policySelect").empty();
			$("#policyEnactedSelect").empty();
			var keys = [];
			if ('policy' in state.d)
			{
				keys = Object.keys(state.d.policy);
			}
			for (var i = 0; i < keys.length; i++)
			{
				if (state.d.policy[keys[i]].enacted == false)
				{
					addOpt(keys[i],$("#policySelect"));
				}
				else
				{
					addOpt(keys[i],$("#policyEnactedSelect"));
				}
			}
			function addOpt(k,parent)
			{
				var o = $('<option> </option>');
				o.attr('id','csPolicy' + String(k));
				o.attr('data-recordID', k);
				o.text(state.d.policy[k].name);
				//$("#policySelect").append(o);
				parent.append(o);
			}
			
			///// policy details
			this.populateAllocateKind();
			
		},
		resetPolicy: function ()
		{
			$('#policyName').val('');
			$('#policyText').val('');
			$('#policyActions').empty();
			$('#policyPointCost').val('');
		},
		addAction: function(type, recID, amount) {
			var s = $('#policyActions');
			var o = $('<option> </option>').text(type + " " + amount + " " + state.d.resource[recID].species);
			o.attr('data-type', type);
			o.attr('data-recordID', recID);
			o.attr('data-amount', amount);
			s.append(o);
		},
		recordFromInput: function() { //generates a policy record object from the data in the form
			var rCreated = {};
			var rLiquidated = {};
			var rSpent = {};
			$('#policyActions option').each(function () {
				var id = $(this).attr('data-recordID');
				var actType = $(this).attr('data-type');
				var amount = parseInt($(this).attr('data-amount'));
				var species = state.d.resource[id].species;
				if (!(isNaN(amount)))
				{
					//console.log('
					if (actType == 'Buy')
					{
						if (!(species in rCreated))
						{
							rCreated[species] = 0;
						}
						rCreated[species] = rCreated[species] + amount;
					}
					else if (actType == 'Liquidate')
					{
						if (!(species in rLiquidated))
						{
							rLiquidated[species] = 0;
						}
						rLiquidated[species] = rLiquidated[species] + amount;
					}
					else if (actType == 'Allocate')
					{
						if (!(species in rSpent))
						{
							rSpent[species] = 0;
						}
						rSpent[species] = rSpent[species] + amount;
					}
					else
					{
						//console.log('warning: unrecognized policy action type: ' + actType);
					}
				}
				//rCreated = {'candy': parseInt($('#candyBought').val())};
				//alert($(this).val());
			});
			var pol = new csPolicy(gameData.self.abbr, [gameData.self.country], $('#policyText').val(), $('#policyName').val(), rCreated, rLiquidated, rSpent, false);
			//console.log(pol);
			return pol;
		},
		updatePointCost: function () { //updates the "policyPointCost" field to reflect the listed actions
			var check = state.policy.check(views.policies.recordFromInput(),state);
			$('#policyPointCost').val(check.cost);
		},
		populateAllocateKind: function () {
			//console.log($('#policyActionSelect').val());
			var s = $('#policyAllocateKind');
			s.empty();
			if( $('#policyActionSelect').val() == 'Buy')
			{
				var recList = state.getIDs('resource',{canCreate: true});
				
				for (var i = 0; i < recList.length; i++)
				{
					s.append($('<option>' + state.d.resource[recList[i]].species + '</option>').attr('data-recordID', recList[i]));
				}
			}
			else if( $('#policyActionSelect').val() == 'Liquidate')
			{
				var recList = state.getIDs('resource',{canLiquidate: true});
				for (var i = 0; i < recList.length; i++)
				{
					s.append($('<option>' + state.d.resource[recList[i]].species + '</option>').attr('data-recordID', recList[i]));
				}
			}
			else if( $('#policyActionSelect').val() == 'Allocate')
			{
				var recList = state.getIDs('resource',{canSpend: true});
				for (var i = 0; i < recList.length; i++)
				{
					s.append($('<option>' + state.d.resource[recList[i]].species + '</option>').attr('data-recordID', recList[i]));
				}
			}
		},
		update: function(action, type, id, field) {
			if (type == 'policy')
			{
				if (action == 'update')
				{
					if ($('#csPolicy' + id).length > 0) //the policy previously existed but has been replaced
					{						// (probably a rare scenario)
						$('#csPolicy' + id).text(state.d.policy[id].name);
					}
					else
					{
						var o = $('<option> </option>');
						o.attr('id','csPolicy' + String(id));
						o.attr('data-recordID', id);
						o.text(state.d.policy[id].name);
						if (state.d.policy[id].enacted == false)
						{
							$('#policySelect').append(o);
						}
						else
						{
							$('#policyEnactedSelect').append(o);
						}
					}
				}
				else if (action == 'updateField') //probably won't ever happen... but just in case...
				{
					if (field == 'name')
					{
						if ($('#csPolicy' + id).length > 0) 
						{
							$('#csPolicy' + id).text(state.d.policy[id].name);
						}
					}
				}
				else if (action == 'remove')
				{
					if ($('#csPolicy' + id).length > 0)
					{						
						$('#csPolicy' + id).remove();
					}
				}
			}
			else
			{
				//do nothing, all this pane cares about is policies
			}
		}
	},
	military:
	{
		initialize: function() {
			//Deploying buttons
			$("#militaryDeployingViewButton").click(function() {
				militaryViewButton('Deploying');
			});
			$('#militaryDeployingCancelButton').click(militaryDeployingCancelButton);
			
			//Active buttons
			$("#militaryActiveViewButton").click(function() {
				militaryViewButton('Active');
			});
			$('#militaryActiveWithdrawButton').click(militaryActiveWithdrawButton);

			//Withdrawing buttons
			$("#militaryWithdrawingViewButton").click(function() {
				militaryViewButton('Withdrawing');
			});
			$('#militaryWithdrawCancelButton').click(militaryWithdrawCancelButton);			
			
			//Other buttons
			$('#militaryRemoveUnitsButton').click(militaryRemoveUnitsButton);
			$('#militaryAddUnitButton').click(militaryAddUnitButton);
			$('#militaryDeployButton').click(militaryDeployButton);
			$('#militaryResetButton').click(militaryResetButton);
			
			// Lists
			views.military.refreshLists();
			
			//// add units to the drop down list
			this.populateUnitKind();
			
			/////// Button Code ///////
			function militaryViewButton(whatList)
			{ //sets the right hand information to the top selected action
				var selected = [];
				var boxTag = "#military" + whatList + "Select";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.military.resetAction();
					var act = state.d.military[selected[0]];
					$('#militaryName').val(act.name);
					$('#militaryDestination').val(act.destination);
					$('#militaryPublicReason').val(act.publicReason);
					$('#militaryRealReason').val(act.realReason);
					
					for (key in act.unitsAllocated)
					{
						views.military.addUnit(state.resource.idFromSpecies(key,state),act.unitsAllocated[key]);
					}
				}
			}
			
			function militaryDeployingCancelButton()
			{//data.type, data.id, data.field, data.value
				var selected = [];
				$("#militaryDeployingSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'military', owner: gameData.self.abbr};
					socketOut('remove', req);
				}
			}
			
			function militaryActiveWithdrawButton()
			{
				var selected = [];
				$("#militaryActiveSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'military', field: 'status', value: 'withdrawing'};
					socketOut('modify', req);
				}
			}
			
			function militaryWithdrawCancelButton()
			{
				var selected = [];
				$("#militaryWithdrawingSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'military', field: 'status', value: 'active'};
					socketOut('modify', req);
				}
			}
			
			function militaryRemoveUnitsButton() {
				var selected = [];
				$("#militaryAllocations option:selected").each(function () {
					$(this).remove();
				});
			}
			
			function militaryAddUnitButton() {
				if(!isNaN(parseInt($('#militaryUnitNumber').val())))
				{
					var selected = [];
					$("#militaryUnitKind option:selected").each(function () {
						selected.push($(this).attr('data-recordID'));
					});
					for (var i = 0; i < selected.length; i++)
					{
						views.military.addUnit(selected[i], parseInt($('#militaryUnitNumber').val()));
					}
				}
			}
			
			function militaryDeployButton() {
				// TODO: Add some validation here
				var action  = views.military.actionFromView();
				
				socketOut('add',action);
				//views.military.resetAction();
			}
			
			function militaryResetButton() {
				views.military.resetAction();
			}
		},
		refreshLists: function()
		{
			///// Action Lists Update
			$("#militaryDeployingSelect").empty();
			$("#militaryActiveSelect").empty();
			$("#militaryWithdrawingSelect").empty();
			var keys = [];
			if ('military' in state.d)
			{
				keys = Object.keys(state.d.military);
			}
			for (var i = 0; i < keys.length; i++)
			{
				if (state.d.military[keys[i]].status == 'deploying')
				{
					addOpt(keys[i],$("#militaryDeployingSelect"));
				}
				else if (state.d.military[keys[i]].status == 'active')
				{
					addOpt(keys[i],$("#militaryActiveSelect"));
				}
				else if (state.d.military[keys[i]].status == 'withdrawing')
				{
					addOpt(keys[i],$("#militaryWithdrawingSelect"));
				}
			}
			function addOpt(k,parent)
			{
				var o = $('<option> </option>');
				o.attr('id','csMilitary' + String(k));
				o.attr('data-recordID', k);
				o.text(state.d.military[k].name);
				parent.append(o);
			}
			

		},
		update: function(action, type, id, field) {
			if (type == 'military')
			{
				if (action == 'update')
				{
					if ($('#csMilitary' + id).length > 0) //the policy previously existed but has been replaced
					{						// (probably a rare scenario)
						$('#csMilitary' + id).text(state.d.military[id].name);
					}
					else
					{
						var o = $('<option> </option>');
						o.attr('id','csMilitary' + String(id));
						o.attr('data-recordID', id);
						o.text(state.d.military[id].name);
						if (state.d.military[id].status == 'deploying')
						{
							$("#militaryDeployingSelect").append(o);
						}
						else if (state.d.military[id].status == 'active')
						{
							$("#militaryActiveSelect").append(o);
						}
						else if (state.d.military[id].status == 'withdrawing')
						{
							$("#militaryWithdrawingSelect").append(o);
						}
					}
				}
				else if (action == 'updateField') 
				{
					if (field == 'name')//probably won't ever happen... but just in case...
					{
						if ($('#csMilitary' + id).length > 0) 
						{
							$('#csMilitary' + id).text(state.d.policy[id].name);
						}
					}
					else if (field == 'status')
					{
						views.military.refreshLists();
					}
				}
				else if (action == 'remove')
				{
					if ($('#csMilitary' + id).length > 0) //the action is being removed
					{
						$('#csMilitary' + id).remove();
					}
				}
			}
			else
			{
				//do nothing, all this pane cares about is military moves
			}
		},
		populateUnitKind: function() {
			//console.log($('#policyActionSelect').val());
			var s = $('#militaryUnitKind');
			s.empty();
			var recList = state.getIDs('resource',{tag: 'military'});
			for (var i = 0; i < recList.length; i++)
			{
				s.append($('<option>' + state.d.resource[recList[i]].species + '</option>').attr('data-recordID', recList[i]));
			}
		},
		resetAction: function ()
		{
			$('#militaryName').val('');
			$('#militaryDestination').val('');
			$('#militaryPublicReason').val('');
			$('#militaryRealReason').val('');
			$('#militaryUnitNumber').val('');
			$('#militaryAllocations').empty();
		},
		addUnit: function(recID, amount) {
			var s = $('#militaryAllocations');
			var o = $('<option> </option>').text(amount + " " + state.d.resource[recID].species);
			o.attr('data-recordID', recID);
			o.attr('data-amount', amount);
			s.append(o);
		},
		actionFromView: function() { //generates a military record object from the data in the form
			var rAllocated = {};
			$('#militaryAllocations option').each(function () {
				var id = $(this).attr('data-recordID');
				var amount = parseInt($(this).attr('data-amount'));
				var species = state.d.resource[id].species;
				if (!(isNaN(amount)))
				{
					if (!(species in rAllocated))
					{
						rAllocated[species] = 0;
					}
					rAllocated[species] = rAllocated[species] + amount;
				}
				//alert($(this).val());
			});
			//csMilitary (sender, targets, name, destination, publicReason, realReason, unitsAllocated, status)
			var act = new csMilitary(gameData.self.abbr, [gameData.self.country], $('#militaryName').val(), $('#militaryDestination').val(), $('#militaryPublicReason').val(), $('#militaryRealReason').val(), rAllocated);
			//console.log(act);
			return act;
		}
	},
	makeoffer:
	{
		initialize: function() {
			//// Button hooks ////
			$('#makeofferActiveViewButton').click(makeofferActiveViewButton);
			$('#makeofferActiveDeleteButton').click(makeofferActiveDeleteButton);
			$('#makeofferAllButton').click(makeofferAllButton);
			$('#makeofferNoneButton').click(makeofferNoneButton);
			$('#makeofferPartnersButton').click(makeofferPartnersButton);
			$('#makeofferDeleteTermsButton').click(makeofferDeleteTermsButton);
			$('#makeofferAddButton').click(makeofferAddButton);
			$('#makeofferSubmitButton').click(makeofferSubmitButton);
			$('#makeofferResetButton').click(makeofferResetButton);
			//// select box hook ////
			$('#makeofferInOutSelect').change(makeofferInOutSelect).change(); //call it right at the beginning just to make sure the right menu is showing
			
			
			//// populate the Active Offers area ////
			if ('trade' in state.d)
			{

				for (var rTrade in state.d.trade)
				{
					// console.log(countryFromAbbreviation(state.d.trade[rTrade].sender));
					// console.log(gameData.self.country);
					if (countryFromAbbreviation(state.d.trade[rTrade].sender) == gameData.self.country) //&& state.d.trade[rTrade].accepted == '')
					{
						//trade originated from this country, and no-one has accepted it.
						views.makeoffer.addActiveOffer(rTrade);
					}
				}	
			}
			
			
			//// populate the Targets check box area ////
			var countries = listCountries();
			var s = $('#makeofferTargetsDiv');
			for (var country in countries)
			{
				if (country != gameData.self.country)
				{
					var o = $('<input/>').attr({id: "makeofferCbx" + country, type: 'checkbox', 'Data-country': country});
					s.append(o);
					s.append($('<label/>').attr({'for': "makeofferCbx" + country}).text(country));
				}
			}
			
			//// populate the resources drop downs ////
			var outSelect = $('#makeofferAllocateKindOut');
			var inSelect = $('#makeofferAllocateKindIn');
			for (var resource in state.d.resource)
			{
				if (state.d.resource[resource].canBuy)
				{
					var o = $('<option> </option>').text(state.d.resource[resource].species).attr('data-recordID',resource);
					outSelect.append(o);
				}
				
				if (state.d.resource[resource].canSell)
				{
					var o = $('<option> </option>').text(state.d.resource[resource].species).attr('data-recordID',resource);
					inSelect.append(o);
				}
			}
			
			//reset the view to default
			views.makeoffer.reset();
			makeofferPartnersButton();
			
			//// Button code ////
			function makeofferActiveViewButton()
			{
				var selected = [];
				var boxTag = "#makeofferActiveSelect";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.makeoffer.reset();
					var trade = state.d.trade[selected[0]];
					for (i in trade.recipients)
					{
						$('#makeofferCbx' + trade.recipients[i]).each(function ()
						{
							this.checked = true;
						});
					}
					
					for (i in trade.incoming)
					{
						views.makeoffer.addTerm('In', trade.incoming[i], state.getIDs('resource', {species: i})[0]);
					}
					
					for (i in trade.outgoing)
					{
						views.makeoffer.addTerm('Out', trade.outgoing[i], state.getIDs('resource', {species: i})[0]);
					}
					if (trade.buyToMeetDemand)
					{
						$('#makeofferCbxBuyToMeetDemand').attr('checked',true);
					}
					$('#makeofferOfferNumber').val(trade.tradesLeft);
				}
			}
			
			function makeofferActiveDeleteButton()
			{
				var selected = [];
				$("#makeofferActiveSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'trade', owner: gameData.self.abbr};
					socketOut('remove', req);
				}				
			}
			
			function makeofferAllButton()
			{
				$('#makeofferTargetsDiv').children('input').each( function () {
					$(this).attr('checked', true);
				});
			}

			function makeofferNoneButton()
			{
				$('#makeofferTargetsDiv').children('input').each( function () {
					$(this).attr('checked', false);
				});
			}			
			
			function makeofferPartnersButton()
			{
				$("#makeofferTargetsDiv").children('input').each( function () {
					if (state.relation.tradePartners(gameData.self.country,$(this).attr('Data-country'),state))
					{
						$(this).attr('checked', true);
					}
					else
					{
						$(this).attr('checked', false);
					}
				});
			}			
			
			function makeofferDeleteTermsButton()
			{
				$("#makeofferTerms option:selected").each( function() 
				{
					$(this).remove();
				});
			}			
			
			function makeofferAddButton()
			{
				var resourceID = '';
				var direction = '';
				var amount = 0;
				var valid = true;
				
				direction = $('#makeofferInOutSelect option:selected').text();
				
				if (direction == 'Out')
				{
					// $("#makeofferAllocateKindOut option:selected").each(function ()
					// {
						// resourceID = $(this).attr('data-recordID'); 
					// });
					resourceID = $("#makeofferAllocateKindOut option:selected").first().attr('data-recordID');
				}
				else if (direction == 'In')
				{
					// $("#makeofferAllocateKindIn option:selected").each(function ()
						// {
							// resourceID = $(this).attr('data-recordID');
						// });
					resourceID = $("#makeofferAllocateKindIn option:selected").first().attr('data-recordID');
				}
				else
				{
					valid = false;
				}
				
				if (parseInt($('#makeofferUnitNumber').val())  > 0)
				{
					amount = parseInt($('#makeofferUnitNumber').val());
				}
				else
				{
					valid = false;
				}
				
				if (valid == true)
				{
					views.makeoffer.addTerm(direction, amount, resourceID);
				}
			}
			
			function makeofferSubmitButton()
			{
				var trade = views.makeoffer.recordFromInput();
				socketOut('add',trade);
				//console.log(trade);
			}
			
			function makeofferResetButton()
			{
				views.makeoffer.reset();
				makeofferPartnersButton();
			}
			
			//// Select Box code ////
			function makeofferInOutSelect()  //show the right select box to match the In/Out selection
			{
				$('#makeofferInOutSelect option:selected').each( function () {
					if($(this).text() == 'Out')
					{
						$("#makeofferAllocateKindOut").show();
						$("#makeofferAllocateKindIn").hide();
					}
					else
					{
						$("#makeofferAllocateKindOut").hide();
						$("#makeofferAllocateKindIn").show();
					}
				});
			}
			

		},
		recordFromInput: function() {
			var rOut = {};
			var rIn = {};
			var directionKey = {Out:-1,In:1};
			var net = {}; //object where key is species name, value is the net amount of that species that will enter the proposing country if the trade is accepted
			var targets = [];
			var recipients = [];
			var numberOfTrades = 1;
			var buyToMeetDemand = $('#makeofferCbxBuyToMeetDemand').is(':checked');
			
			var valid = true;
			
			
			$('#makeofferTerms option').each(function() {
				var id = $(this).attr('data-recordID');
				var direction = directionKey[$(this).attr('data-direction')];
				var amount = parseInt($(this).attr('data-amount'));
				var species = state.d.resource[id].species;
				if (parseInt(amount) > 0)
				{
					if (!(species in net))
					{
						net[species] = 0;
					}
					net[species] += direction * amount;
				}
				else 
				{
					valid = false;
				}	
			});
			
			if (valid == false)
			{
				chatAdd('err', 'error: nonpositive value in trade offer terms');
			}
			
			$("#makeofferTargetsDiv input:checked").each(function() {
				recipients.push($(this).attr('data-country'));
			});
			
			targets = recipients.slice();
			targets.unshift(gameData.self.country);
			
			for (var species in net)
			{
				if (net[species] > 0)
				{
					rIn[species] = net[species];
				}
				else if (net[species] < 0)
				{
					rOut[species] = -1 * net[species];
				}
			}
			
			
			numberOfTrades = parseInt($('#makeofferOfferNumber').val());
			if (numberOfTrades < 1)
			{
				valid = false;
				chatAdd('err', 'error: non-positive value in number of trades');
			}
			
			
			//(sender, targets, recipients, outgoing, incoming)
			var rec = new csTrade(gameData.self.abbr, targets, recipients, rOut, rIn, buyToMeetDemand, numberOfTrades);
			return rec;
		},
		addActiveOffer: function (id)
		{
			var maxSummaryLength = 25;
			var summary = '';
			//console.log(id);
			
			for (var i in state.d.trade[id].outgoing)
			{
				summary += 'O'+ state.d.trade[id].outgoing[i] + i.slice(0,3) + ' ';
			}
			for (var i in state.d.trade[id].incoming)
			{
				summary += 'I'+ state.d.trade[id].incoming[i] + i.slice(0,3) + ' ';
			}
			
			if (summary.length > maxSummaryLength - 3)
			{
				summary = summary.slice(0, maxSummaryLength - 4) + '...';
			}
			if (summary.length == 0)
			{
				summary = 'null';
			}
			
			var s = $('#makeofferActiveSelect');
			var o = $('<option/>').attr({'id': 'csTrade' + id, 'data-recordID': id}).text(summary);
			s.append(o);
		},
		addTerm: function (direction, amount, resourceID)
		{
			var s = $('#makeofferTerms');
			var o = $('<option> </option>').text(direction + " " + amount + " " + state.d.resource[resourceID].species);
			o.attr('data-direction', direction);
			o.attr('data-recordID', resourceID);
			o.attr('data-amount', amount);
			s.append(o);
		},
		reset: function ()
		{
			$('#makeofferTerms').children().each( function () {
				$(this).remove();
			});
			$('#makeofferTargetsDiv > input').each( function () {
				//console.log($(this).attr('data-country'));
				this.checked = false;
			});
			$('#makeofferCbxBuyToMeetDemand').attr('checked', false);
			$('#makeofferOfferNumber').val('1');
		},
		update: function(action, type, id, field, value) {
			//console.log(action + " " + type + " " + id);
			if (type == 'trade')
			{
				if (action == 'update')
				{
					// console.log(countryFromAbbreviation(state.d.trade[id].sender));
					// console.log(gameData.self.country);
					if (countryFromAbbreviation(state.d.trade[id].sender) == gameData.self.country )//(&& state.d.trade[id].accepted == '')
					{
						views.makeoffer.addActiveOffer(id);
					}
				}
				else if (action == 'remove')
				{
					//console.log('remove');
					$('#csTrade' + String(id)).remove();
				}
			}
		}
		
	},
	viewoffers:
	{
		initialize: function() {
			//// button hooks ////
			$('#viewoffersViewButton').click(viewoffersViewButton);
			$('#viewoffersAcceptButton').click(viewoffersAcceptButton);
			$('#viewoffersClearButton').click(viewoffersClearButton);
			
			//// reset the view and add whatever trades are in state.d.trade and have not been accepted, and do not have self as the sender ///// 
			views.viewoffers.reset();
			if ('trade' in state.d)
			{
				for (var i in state.d.trade)
				{
					if (countryFromAbbreviation(state.d.trade[i].sender) != gameData.self.country)
					{
						views.viewoffers.addOffer(i);
					}
				}
			}
			//// button code ////
			function viewoffersViewButton()
			{
				var selected = [];
				var boxTag = "#viewoffersSelect";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.viewoffers.reset();
					views.viewoffers.openedOffer = selected[0];
					
					var trade = state.d.trade[selected[0]];
					
					if (!state.relation.tradePartners(gameData.self.country, countryFromAbbreviation(trade.sender), state))
					{
						$('#viewoffersSourceWarning').show();
					}	
					
					
					$('#viewoffersAcceptButton').show();
					

					
					$('#viewoffersSource').text(countryFromAbbreviation(trade.sender));
					
					for (i in trade.incoming)
					{
						views.viewoffers.addTerm('In', trade.incoming[i], state.getIDs('resource', {species: i})[0]);
					}
					
					for (i in trade.outgoing)
					{
						views.viewoffers.addTerm('Out', trade.outgoing[i], state.getIDs('resource', {species: i})[0]);
					}
				}
			}
			
			function viewoffersAcceptButton()  // A trade is accepted by someone other than the sender country making a remove request
			{
				if (views.viewoffers.openedOffer != -1)
				{
					var req = {id: views.viewoffers.openedOffer, type:'trade', field: 'tradesLeft', value: state.d.trade[views.viewoffers.openedOffer].tradesLeft - 1};
					socketOut('modify', req);
				}
			}
			
			function viewoffersClearButton()
			{
				views.viewoffers.reset();
			}
			
		},
		openedOffer: -1,
		reset: function ()
		{
			$('#viewoffersAcceptButton').hide();
			views.viewoffers.openedOffer = -1;
			$('#viewoffersSource').text('');
			$('#viewoffersSourceWarning').hide();
			$('#viewoffersTerms option').each(function () {
				$(this).remove();
			});
		},
		addTerm: function (direction, amount, resourceID)
		{
			var s = $('#viewoffersTerms');
			var o = $('<option> </option>').text(direction + " " + amount + " " + state.d.resource[resourceID].species);
			o.attr('data-direction', direction);
			o.attr('data-recordID', resourceID);
			o.attr('data-amount', amount);
			s.append(o);
		},
		addOffer: function (id)
		{
			var maxSummaryLength = 80;
			var summary = '';
			//console.log(id);
			summary += countryFromAbbreviation(state.d.trade[id].sender).slice(0,4) + ": ";
			
			for (var i in state.d.trade[id].outgoing)
			{
				summary += 'O'+ state.d.trade[id].outgoing[i] + i.slice(0,3) + ' ';
			}
			for (var i in state.d.trade[id].incoming)
			{
				summary += 'I'+ state.d.trade[id].incoming[i] + i.slice(0,3) + ' ';
			}
			
			if (summary.length > maxSummaryLength - 3)
			{
				summary = summary.slice(0, maxSummaryLength - 4) + '...';
			}
			if (summary.length == 0)
			{
				summary = 'null';
			}
			
			var s = $('#viewoffersSelect');
			var o = $('<option/>').attr({'id': 'csTrade' + id, 'data-recordID': id}).text(summary);
			s.append(o);
		},
		
		update: function(action, type, id, field, value) {
			if (type == 'trade')
			{
				if (action == 'update')
				{
					if (countryFromAbbreviation(state.d.trade[i].sender) != gameData.self.country)
					{
						views.viewoffers.addOffer(id);
					}
				}
				else if (action == 'modify')
				{
					//TODO: make it change the name to account for the number of offers left.
				}
				else if (action == 'remove')
				{
					$('#csTrade' + String(id)).remove();
					if (id == views.viewoffers.openedOffer)
					{
						views.viewoffers.reset();
					}
				}
			}
			else if (type == 'relation')
			{
				if (views.viewoffers.openedOffer != -1)
				{
					//if an embassy is established or removed, update the view trade embassy warning (just in case a trade from the country in question is opened)
					if (!state.relation.tradePartners(gameData.self.country, countryFromAbbreviation(state.d.trade[views.viewoffers.openedOffer].sender), state))
					{
						$('#viewoffersSourceWarning').show();
					}
					else
					{
						$('#viewoffersSourceWarning').hide();
					}
				}
			}
		}
	},
	resources:
	{
		initialize: function() {
			for (var key in state.d.resource)
			{
				views.resources.add(key);
				//$('#resourcesTable').append($('<tr><td></td></tr>').val('X'));
			}
			views.resources.refreshColors();
		},
		add: function(id)
		{
			// name start current minimum cost scrap
			var tooltips  ={'navy':'Military on the sea.',
							'army':'Ground troops.',
							'points':'Game currency; used to buy units/trade.',
							'R&D':'your scientists who work to find better technology for your military and economy. Very important to development of your country. This is also your intelligence. (allocated in policies)',
							'diplomacy':'the ability your country has to negotiate trades, treaties, wars, and power in memberships. This is also representative of your embassies abroad. (allocated in embassies)',
							'edibulls':'agricultural products which may include grains, fruits, vegetables, silage, spices, etc.',
							'scrapdus':'raw and natural materials which may include timber, iron, coal, water, precious metals, etc.',
							'blista':'all liquid fuel. Hard to find and pollutant when burned.',
							'greeslik':'prepared foods. Foods already processed for consumption.',
							'krust':'all solid fuel sources. Highly pollutant when burned and hard on the environment to excavate.',
							'finosh':'manufactured goods which may include machinery, clothing, transportation, etc.',
							'treasury':'the amount of money (in the form of units) you have to spend on domestic and foreign issues, programs, etc. (allocated in policies)',
							'economy':'the general health of the economic status of your country, which includes manufacturing, education, resources, labor unrest, trade, tariffs, and taxes.',
							'people':'the general status of the happiness of your people.',
							'health':'the general status of the health of your country. Also represents the medical technology your country possesses.',
							'culture':'the arts and liberal arts such as painters, musicians, writers, sports, etc.'}
							
			var o = $('<tr>');
			//o.append($('<td> ').text(id));
			o.append($('<td>').text(state.d.resource[id].species).attr({id:'csResource' + String(id) + "species",title:tooltips[state.d.resource[id].species]}));
			o.append($('<td>').text(state.d.resource[id].start).attr('id','csResource' + String(id) + "start"));
			o.append($('<td>').text(state.d.resource[id].current).attr('id','csResource' + String(id) + "current"));
			o.append($('<td>').text(state.d.resource[id].minimum).attr('id','csResource' + String(id) + "minimum"));
			if (state.d.resource[id].canCreate == true) //if it can be created, show the cost
			{
				o.append($('<td>').text(state.d.resource[id].createCost).attr('id','csResource' + String(id) + "createCost"));
			}
			else // else just leave that box blank
			{
				o.append($('<td>').attr('id','csResource' + String(id) + "createCost"));
			}
			
			if (state.d.resource[id].canLiquidate == true) //if it can be scrapped, show the value
			{
				o.append($('<td>').text(state.d.resource[id].liquidateCost).attr('id','csResource' + String(id) + "liquidateCost"));
			}
			else // else just leave that box blank
			{
				o.append($('<td>').attr('id','csResource' + String(id) + "liquidateCost"));
			}
			$('#resourcesTableBody').append(o);
		},
		refreshColors: function()
		{
			for (var record in state.d.resource)
			{
				if (state.d.resource[record].current < state.d.resource[record].minimum)
				{
					$('#csResource' + record + 'current').removeClass('above_min');
					$('#csResource' + record + 'current').removeClass('at_min');
					$('#csResource' + record + 'current').addClass('below_min');
				}
				else if (state.d.resource[record].current > state.d.resource[record].minimum)
				{
					$('#csResource' + record + 'current').removeClass('below_min');
					$('#csResource' + record + 'current').removeClass('at_min');
					$('#csResource' + record + 'current').addClass('above_min');				
				}
				else
				{
					$('#csResource' + record + 'current').removeClass('below_min');
					$('#csResource' + record + 'current').removeClass('above_min');
					$('#csResource' + record + 'current').addClass('at_min');				
				}
			}
		},
		update: function(action, type, id, field) {
			if (type == 'resource')
			{
				if (action == 'update')
				{
					if ($('#csResource' + id + "species").length > 0) //record already exists, so update all the fields individually
					{
						var fields = ["species","start","current","minimum","createCost","liquidateCost"];
						fields.forEach(function(field)
						{
							views.resources.update('updateField', 'resource', id, field);
						});
					}
					else	//otherwise just add it
					{
						view.resources.add(id);
					}
				}
				else if (action == 'updateField') 
				{
					$('#csResource' + String(id) + field).text(state.d.resource[id][field])
				}
				else if (action == 'remove')
				{
					$('#csResource' + String(id) + 'species').parent().remove();
				}
				views.resources.refreshColors();
			}
		}
	},
	taxes:
	{
		initialize: function() {
			
			//// Button hooks ////
			$('#taxesViewButton').click(taxesViewButton);
			$('#taxesDeleteButton').click(taxesDeleteButton);
			$('#taxesResetButton').click(taxesResetButton);
			$('#taxesEnactButton').click(taxesEnactButton);
			
			for (var key in state.d.tax) //populate the list
			{
				views.taxes.add(key);
			}
			
			//// Button Code ////
			function taxesViewButton()
			{
				var selected = [];
				var boxTag = "#taxesSelect";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.taxes.reset();
					var tax = state.d.tax[selected[0]];
					$('#taxesName').val(tax.name);
					$('#taxesText').val(tax.text);
					$('#taxesTargetRevenue').val(tax.points);
				}
			}
			
			function taxesDeleteButton()
			{
				var selected = [];
				$("#taxesSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var req = {id: selected[i], type:'tax', owner: gameData.self.abbr};
					socketOut('remove', req);
				}
			}

			function taxesResetButton()
			{
				views.taxes.reset();
			}			
			
			function taxesEnactButton()
			{
				// TODO: Add some validation here
				var tax  = views.taxes.fromView();
			
				socketOut('add',tax);
				//views.taxes.reset();
			}
			
			views.taxes.refreshExpectedRevenue();
		},
		update: function(action, type, id, field) {
			if (type == 'tax')
			{
				if (action == 'update')
				{
					if ($('#csTax' + id).length > 0) //record already exists, so just refresh the name and pts
					{
						views.taxes.update('updateField', 'tax', id);
					}
					else	//otherwise just add it
					{
						views.taxes.add(id);
					}
				}
				else if (action == 'updateField')  //if any field is updated, just refresh the whole label text
				{
					$('#csTax' + String(id)).text(state.d.tax[id].points + " " + state.d.tax[id].name);
				}
				else if (action == 'remove')
				{
					$('#csTax' + String(id)).remove();
				}
				views.taxes.refreshExpectedRevenue();
			}
		},
		refreshExpectedRevenue: function()
		{
			var totalRevenue = 0;
			if ('tax' in state.d)
			{
				for (var itax in state.d.tax)
				{
					totalRevenue += parseInt(state.d.tax[itax].points);
				}
			}
			$('#taxesTotalExpected').text(totalRevenue);
		},
		fromView: function()
		{
			var tax = new csTax(gameData.self.abbr, [gameData.self.country], $('#taxesName').val(), $('#taxesText').val(), $('#taxesTargetRevenue').val())
			
			return tax;
		},
		add: function(id)
		{
			var s = $('#taxesSelect');
			var o = $('<option> </option>').text(state.d.tax[id].points + " " + state.d.tax[id].name);
			o.attr('id', 'csTax' + id);
			o.attr('data-recordID', id);
			//o.attr('data-amount', amount);
			s.append(o);
		},
		reset: function()
		{
			$('#taxesName').val('');
			$('#taxesText').val('');
			$('#taxesTargetRevenue').val('');
		}
	},
	treaties:
	{
		initialize: function() {
			//// Button hooks ////
			$('#treatiesSignedViewButton').click(treatiesSignedViewButton);
			$('#treatiesPendingViewButton').click(treatiesPendingViewButton);
			$('#treatiesSignedWithdrawButton').click(treatiesSignedWithdrawButton);
			$('#treatiesPendingDeclineButton').click(treatiesPendingDeclineButton);
			$('#treatiesSubmitButton').click(treatiesSubmitButton);
			$('#treatiesSignButton').click(treatiesSignButton);
			$('#treatiesNewButton').click(treatiesNewButton);
			$('#treatiesLeftArrowButton').click(treatiesLeftArrowButton);
			$('#treatiesRightArrowButton').click(treatiesRightArrowButton);
			
			for (var id in state.d.treaty) //populate the lists
			{
				views.treaties.add(id);
			}
			
			views.treaties.viewStyle('new');
			
			//// Button Code ////
			function treatiesSignedViewButton()
			{
				var selected = [];
				var boxTag = "#treatiesSignedSelect";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.treaties.showTreaty(selected[0]);
				}
			}
			
			function treatiesPendingViewButton()
			{
				var selected = [];
				var boxTag = "#treatiesPendingSelect";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					//views.treaties.viewStyle('pending');
					views.treaties.showTreaty(selected[0]);
				}
			}
			
			function treatiesSignedWithdrawButton()
			{
				var selected = [];
				$("#treatiesSignedSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var newStatus = state.d.treaty[selected[i]].status;
					
					var myIndex = state.treaty.getCountryIndex(gameData.self.country,state.d.treaty[selected[i]]);
					newStatus[myIndex] = 'withdrawn';
					var req = {id: selected[i], type:'treaty', field: 'status', value: newStatus};
					socketOut('modify', req);
				}
			}
			
			function treatiesPendingDeclineButton()
			{
				var selected = [];
				$("#treatiesPendingSelect option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				for (var i = 0; i < selected.length; i++)
				{
					var newStatus = state.d.treaty[selected[i]].status;
					var myIndex = state.treaty.getCountryIndex(gameData.self.country,state.d.treaty[selected[i]]);
					newStatus[myIndex] = 'declined';
					var req = {id: selected[i], type:'treaty', field: 'status', value: newStatus};
					socketOut('modify', req);
				}
			}
			
			function treatiesSubmitButton()
			{
				var treaty = views.treaties.fromView();
				
				socketOut('add', treaty);
			}
			
			function treatiesSignButton()
			{
				var id = views.treaties.showing;
				if (views.treaties.showing != -1)
				{
					var newStatus = state.d.treaty[id].status;
					var myIndex = state.treaty.getCountryIndex(gameData.self.country,state.d.treaty[id]);
					newStatus[myIndex] = 'signed';
					var req = {id: id, type:'treaty', field: 'status', value: newStatus};
					socketOut('modify', req);
				}
			}
			
			function treatiesNewButton()
			{
				views.treaties.showing = -1;
				views.treaties.viewStyle('new');
			}
			
			function treatiesLeftArrowButton()
			{
				$("#treatiesPartiesSelect option:selected").each(function () {
					if($(this).text() != gameData.self.country)  //you can't remove yourself from the parties list
					{
						$("#treatiesCountriesSelect").append($(this));
					}
				});
			}
			
			function treatiesRightArrowButton()
			{
				$("#treatiesCountriesSelect option:selected").each(function () {
					$("#treatiesPartiesSelect").append($(this));
				});
			}
			
		},
		add: function(id)
		{
			var myIndex = state.treaty.getCountryIndex(gameData.self.country,state.d.treaty[id]);
			if (state.d.treaty[id].status[myIndex] == 'signed')
			{
				var s = $('#treatiesSignedSelect');
				var o = $('<option> </option>').text(state.d.treaty[id].name);
				o.attr('id', 'csTreaty' + id);
				o.attr('data-recordID', id);
				o.attr('data-recordList', 'signed');
				s.append(o);
			}
			else if (state.d.treaty[id].status[myIndex] == 'pending')
			{
				var s = $('#treatiesPendingSelect');
				var o = $('<option> </option>').text(state.d.treaty[id].name);
				o.attr('id', 'csTreaty' + id);
				o.attr('data-recordID', id);
				o.attr('data-recordList', 'pending');
				s.append(o);
			}
		},
		remove: function(id)
		{
			$('#csTreaty'+id).remove();
		},
		viewStyle: function(style) //style can be 'signed', 'pending', or 'new'
		{
			$('#treatiesName').val('');
			$('#treatiesText').val('');
			$('#treatiesCountriesSelect').empty();
			$('#treatiesPartiesSelect').empty();
			
			if (style == 'signed' || style == 'pending')
			{
				$('#treatiesName').attr('readonly',true);
				$('#treatiesText').attr('readonly',true);
				$('#treatiesCountriesSelectLabel').attr('style','display: none;');
				$('#treatiesCountriesSelect').attr('style','display: none;');
				$('#treatiesLeftArrowButton').attr('style','display: none;');
				$('#treatiesRightArrowButton').attr('style','display: none;');
				$('#treatiesSubmitButton').attr('style','display: none;');
				$('#treatiesColorKey').attr('style','display: ;');
			}
			else if (style == 'new')
			{
				$('#treatiesName').attr('readonly',false);
				$('#treatiesText').attr('readonly',false);
				$('#treatiesCountriesSelect').attr('style','display: ;');
				$('#treatiesCountriesSelectLabel').attr('style','display: ;');
				$('#treatiesLeftArrowButton').attr('style','display: ;');
				$('#treatiesRightArrowButton').attr('style','display: ;');
				$('#treatiesSubmitButton').attr('style','display: ;');
				$('#treatiesColorKey').attr('style','display: none;');
				
				var countries = listCountries();
				var s = $('#treatiesCountriesSelect');
				for (var country in countries)
				{
					var o = $('<option> </option>').text(country);
					o.attr('id', 'treatiesCountrySelect' + country);
					s.append(o);
				}
				// move Country of player into 'Parties' box, because a treaty cannot be proposed without the proposing country as a member
				$('#treatiesPartiesSelect').append($('#treatiesCountrySelect' + gameData.self.country));
			}
			
			if (style == 'pending')
			{
				$('#treatiesSignButton').attr('style','display: ;');
			}
			else
			{
				$('#treatiesSignButton').attr('style','display: none;');
			}
			
			views.treaties.showing = -1;
		},
		showTreaty: function(id)
		{
			if (id in state.d.treaty)
			{
				var myIndex = state.treaty.getCountryIndex(gameData.self.country,state.d.treaty[id]);
				if (state.d.treaty[id].status[myIndex] == 'pending')
				{
					views.treaties.viewStyle('pending');
				}
				else
				{
					views.treaties.viewStyle('signed');
				}
				
				views.treaties.showing = id;
				$('#treatiesName').val(state.d.treaty[id].name);
				$('#treatiesText').val(state.d.treaty[id].text);
				var s = $('#treatiesPartiesSelect');
				// var o = $('<option> </option>').text(country);
				// o.attr('id', 'treatiesCountrySelect' + country);
				// 
				for (party = 0; party < state.d.treaty[id].targets.length; party++)
				{
					var o = $('<option> </option>').text(state.d.treaty[id].targets[party]);
					if (state.d.treaty[id].status[party] == 'signed')
					{
						o.attr('class','signed');
					}
					else if (state.d.treaty[id].status[party] == 'pending')
					{
						o.attr('class','pending');
					}
					else if (state.d.treaty[id].status[party] == 'withdrawn' || state.d.treaty[id].status[party] == 'declined')
					{
						o.attr('class','withdrawn_declined');
					}
					o.attr('id', 'treatiesPartiesSelect' + id + state.d.treaty[id].targets[party]);
					s.append(o);
				}
			}
			
		},
		showing: -1, //the ID of the currently showing treaty, -1 if a treaty that isn't in the state object is showing (useful for know which treaty is being signed)
		update: function(action, type, id, field, value) {
			if (type == 'treaty')
			{
				if (action == 'update')
				{
					if ($('#csTreaty' + String(id)).length > 0) //record already exists, so remove it, then add it (this should be a rare or impossible occurence)
					{
						views.treaties.remove(id);
						views.treaties.add(id);
					}
					else	//otherwise just add it
					{
						views.treaties.add(id);
					}
				}
				else if (action == 'updateField') 
				{
					var myIndex = state.treaty.getCountryIndex(gameData.self.country,state.d.treaty[id]);
					if(id == views.treaties.showing) //if the currently showing treaty is updated, just refresh the view
					{
						views.treaties.showTreaty(id);
					}
					
					if (field == 'status')
					{
						if ($('#csTreaty'+String(id)).attr('data-recordList') == 'pending')
						{
							if (value[myIndex] == 'signed')
							{
								//console.log('correct Switch');
								$('#treatiesSignedSelect').append($('#csTreaty'+String(id)));
								$('#csTreaty'+String(id)).attr('data-recordList', 'signed');
							}
							else if (value[myIndex] != 'pending')
							{
								//console.log('bad');
								$('#csTreaty'+String(id)).remove();
							}
						}
						else if ($('#csTreaty'+String(id)).attr('data-recordList') == 'signed')
						{
							if (value[myIndex] == 'pending') //should be impossible
							{
								$('#treatiesPendingSelect').append($('#csTreaty'+String(id)));
								$('#csTreaty'+String(id)).attr('data-recordList', 'pending');
							}
							else if (value[myIndex] != 'signed')
							{
								$('#csTreaty'+String(id)).remove();
							}
						}
					}
				}
				else if (action == 'remove')
				{
					//This doesn't seem likely to happen during a turn, so I'm not going to worry about it for now...
				}
			}
		},
		fromView: function() { //generates a treaty record object from the data in the form
			var treaty = new csTreaty(gameData.self.abbr, [], $('#treatiesName').val(), $('#treatiesText').val(), [])
			
			$("#treatiesPartiesSelect option").each(function () {
				treaty.targets.push($(this).text());
				if ($(this).text() == gameData.self.country)
				{
					treaty.status.push('signed');
				}
				else
				{
					treaty.status.push('pending');
				}
			});
			
			return treaty;
		}
	},
	embassies:
	{
		initialize: function() {
			for (var key in state.d.relation)
			{
				views.embassies.addRow(key);

				//$('#resourcesTable').append($('<tr><td></td></tr>').val('X'));
			}
			views.embassies.updateTableButtons();
		},
		yourPosition: function(id)
		{
			var them2us = [0,2,1];
			return them2us[views.embassies.theirPosition(id)];
		},
		theirPosition: function(id)
		{
			var position = -1;
			//we want to find out which of the countries in the record is not the one of the current client
			//console.log(id);
			if (state.d.relation[id].country1 == gameData.self.country)
			{
				position = 2; 
			}
			else if (state.d.relation[id].country2 == gameData.self.country)
			{
				position = 1;
			}
			return position;
		},
		addRow: function(id) //recordID, positionOfOtherCountry
		{
			var pos = views.embassies.theirPosition(id);
			//console.log(pos);
			var them2us = [0,2,1]; //translates positionOfOtherCountry into position of your country
			var o = $('<tr>');
			o.append($('<td>').text(state.d.relation[id]["country" + String(pos)]).attr('id','csRelation' + String(id) + "country"));
			o.append($('<td>').text(state.d.relation[id].status).attr('id','csRelation' + String(id) + "status"));
			o.append($('<td>').text(state.d.relation[id]["unitsIn" + String(pos)] + " " + state.d.relation[id]["embassy" + String(pos)] ).attr('id','csRelation' + String(id) + "unitsThem").attr('data-recordID',id).attr('data-column','unitsThem'));
			o.append($('<td>').text(state.d.relation[id]["unitsIn" + String(them2us[pos])] + " " + state.d.relation[id]["embassy" + them2us[pos]] ).attr('id','csRelation' + String(id) + "unitsYou").attr('data-recordID',id).attr('data-column','unitsYou'));
			//put in the add points remove points buttons
			o.append($('<td>').append($('<button id="csRelation'+ id +'minusOneButton">-1</button>').click(function(){views.embassies.minusOne(id)})).append($('<button id="csRelation'+ id +'plusOneButton">+1</button>').click(function(){views.embassies.plusOne(id)})));
			//o.append($('<td>').attr('data-recordID',id).attr('data-column','units'));
			$('#embassiesTableBody').append(o);
		},
		update: function(action, type, id, field) {
			if (type == 'relation')
			{
				var theirPos = views.embassies.theirPosition(id);
				var yourPos = views.embassies.yourPosition(id);
				
				if (action == 'update')
				{
					if ($('#csRelation' + String(id) + "country").length > 0) //record already exists, so just refresh it
					{
						views.embassies.update('updateField', '', id);
					}
					else	//otherwise just add it
					{
						views.embassies.addRow(id);
					}
				}
				else if (action == 'updateField')  //if any field is updated, just refresh the whole row
				{
					$('#csRelation' + String(id) + "country").text(state.d.relation[id]["country" + String(theirPos)]);
					$('#csRelation' + String(id) + "status").text(state.d.relation[id].status);
					$('#csRelation' + String(id) + "unitsThem").text(state.d.relation[id]["unitsIn" + String(theirPos)] + " " + state.d.relation[id]["embassy" + theirPos]);
					$('#csRelation' + String(id) + "unitsYou").text(state.d.relation[id]["unitsIn" + String(yourPos)] + " " + state.d.relation[id]["embassy" + yourPos]);
					views.embassies.updateTableButtons();
				}
				else if (action == 'remove')
				{
					//This doesn't seem likely to happen during a turn, so I'm not going to worry about it for now...
				}
			}
		},
		updateTableButtons: function()
		{
			//This is kind of a blunt hammer, it may need to be made more specific at some point, depending on how usable it is like this
			
			//first, remove all of the buttons
			$("#embassiesTableBody button[data-embassiesButton='true']").each(function () {
				$(this).remove();
			});
			
			//then add back the ones that should be there
			$("#embassiesTableBody td[data-column='unitsThem']").each(function () {
				var id = $(this).attr('data-recordID');
				if(state.d.relation[id]["embassy" + views.embassies.theirPosition(id)] == "E")
				{
					var b = $('<button>').text('Expel').attr('data-embassiesButton','true').click(function()
						{
							views.embassies.change(id,views.embassies.theirPosition(id),'N');
						});
					$(this).append(b);
				}
				else if(state.d.relation[id]["embassy"+views.embassies.theirPosition(id)] == "R")
				{
					var b = $('<button>').text('Accept').attr('data-embassiesButton','true').click(function()
						{
							views.embassies.change(id,views.embassies.theirPosition(id),'E');
						});
					$(this).append(b);
				}
			});
			
			$("#embassiesTableBody td[data-column='unitsYou']").each(function () {
				var id = $(this).attr('data-recordID');
				if(state.d.relation[id]["embassy"+views.embassies.yourPosition(id)] == "E")
				{
					var b = $('<button>').text('Withdraw').attr('data-embassiesButton','true').click(function()
						{
							views.embassies.change(id,views.embassies.yourPosition(id),'N');
						});
					$(this).append(b);
				}
				else if(state.d.relation[id]["embassy"+views.embassies.yourPosition(id)] == "R")
				{
					var b = $('<button>').text('Cancel').attr('data-embassiesButton','true').click(function()
						{
							views.embassies.change(id,views.embassies.yourPosition(id),'N');
						});
					$(this).append(b);
				}
				else if(state.d.relation[id]["embassy"+views.embassies.yourPosition(id)] == "N")
				{
					var b = $('<button>').text('Request').attr('data-embassiesButton','true').click(function()
						{
							views.embassies.change(id,views.embassies.yourPosition(id),'R');
						});
					$(this).append(b);
				}
			});
		},
		change: function(id, pos, newState)
		{
			//TODO: validation
			var req = {id: id, type:'relation', field: 'embassy'+String(pos), value: newState};
			socketOut('modify', req);
		},
		minusOne: function(id)
		{
			//TODO: validation
			var req = {id: id, type:'relation', field: 'unitsIn'+String(views.embassies.yourPosition(id)), value: state.d.relation[id]["unitsIn" + String(views.embassies.yourPosition(id))] - 1 };
			socketOut('modify', req);
		},
		plusOne: function(id)
		{
			//TODO: validation
			var req = {id: id, type:'relation', field: 'unitsIn'+String(views.embassies.yourPosition(id)), value: state.d.relation[id]["unitsIn" + String(views.embassies.yourPosition(id))] + 1 };
			socketOut('modify', req);
		}
	},
	mail:
	{
		initialize: function() {
			//// Button hooks ////
			$('#mailListViewButton').click(mailListViewButton);
			$('#mailListMarkUnreadButton').click(mailListMarkUnreadButton);
			$('#mailSendButton').click(mailSendButton);
			$('#mailNewButton').click(mailNewButton);
			$('#mailReplyButton').click(mailReplyButton);
			$('#mailReplyAllButton').click(mailReplyAllButton);
			$('#mailLeftArrowButton').click(mailLeftArrowButton);
			$('#mailRightArrowButton').click(mailRightArrowButton);
			
			
			$('#mailListSelect').change( function () {
				views.mail.refreshMultiList();
			}).change(); 

			
			views.mail.viewStyle('new');
			
			//// Button Code ////
			function mailListViewButton()
			{
				var selected = [];
				var boxTag = "#mailMultiSelect";

				$(boxTag + " option:selected").each(function () {
					selected.push($(this).attr('data-recordID'));
				});
				if (selected.length > 0)
				{
					views.mail.showMessage(selected[0]);
					var rec = state.d.mail[selected[0]];
					var readBy = Object.keys(hashify(rec.readBy));
					if ($('#mailListSelect > option:selected').first().text() == 'Unread')
					{
						readBy.push(gameData.self.name);
						var req = {id: selected[0], type:'mail', field: 'readBy', value: readBy};
						socketOut('modify', req);
					}
				}
			}
			
			function mailSendButton()
			{
				var mail = views.mail.fromView();
				
				socketOut('add', mail);
			}

			function mailReplyButton()
			{
				views.mail.showMessage(views.mail.showing, 'reply');
			}
			
			function mailReplyAllButton()
			{
				views.mail.showMessage(views.mail.showing, 'replyAll');
			}
			
			function mailListMarkUnreadButton()
			{
				$('#mailMultiSelect > option:selected').each( function () {
					
					
					var oldArray = state.d.mail[$(this).attr('data-recordID')].readBy;
					var newArray = [];
					for (var userIndex in oldArray)
					{
						if (oldArray[userIndex] != gameData.self.name)
						{
							newArray.push(oldArray[userIndex]);
						}
					}
					
					var req = {id: $(this).attr('data-recordID'), type:'mail', field: 'readBy', value: newArray};
					socketOut('modify', req);
				});
			}
			
			function mailNewButton()
			{
				views.mail.showing = -1;
				views.mail.viewStyle('new');
			}
			
			function mailLeftArrowButton()
			{
				$("#mailRecipientsSelect option:selected").each(function () {
						$("#mailCountriesSelect").append($(this));
				});
			}
			
			function mailRightArrowButton()
			{
				$("#mailCountriesSelect option:selected").each(function () {
					$("#mailRecipientsSelect").append($(this));
				});
			}
			
		},
		add: function(id)
		{
			var option = $('#mailListSelect > option:selected').first().text();		
			var countryTargetsHash = hashify(state.d.mail[id].countryTargets);
			var targetsHash = hashify(state.d.mail[id].targets);
			
			var s = $('#mailMultiSelect');

			if (gameData.self.country in targetsHash)
			{
				if (gameData.self.country in countryTargetsHash)
				{
					// message is to client
					var o = $('<option> </option>').text(state.d.mail[id].name);
					o.attr('id', 'csMail' + id);
					o.attr('data-recordID', id);
					var readByHash = hashify(state.d.mail[id].readBy);
					
					if ((option == 'Unread') && !(gameData.self.name in readByHash))
					{
						// if unread put it in the unread list
						s.append(o);
					}
					else if ((option == 'Read') && (gameData.self.name in readByHash))
					{	
						//if read put it in the read list
						s.append(o);
					}
				}
				if ((option == 'Sent')  && (gameData.self.country == state.d.mail[id].countrySender))
				{
					var o = $('<option> </option>').text(state.d.mail[id].name);
					o.attr('id', 'csMail' + id);
					o.attr('data-recordID', id);
					s.append(o);
				}
			}
		},
		remove: function(id)
		{
			$('#csMail'+id).remove();
		},
		viewStyle: function(style) //style can be 'read', 'new', or 'reply'
		{
			$('#mailName').val('');
			$('#mailText').val('');
			$('#mailCountriesSelect').empty();
			$('#mailRecipientsSelect').empty();
			
			if (style == 'read')
			{
				$('#mailName').attr('readonly',true);
				$('#mailText').attr('readonly',true);
				
				$(".mailNewMail").each( function () {
					$(this).hide();
				});
				
				$(".mailViewMail").each( function () {
					$(this).show();
				});
				
			}
			else if (style == 'new' )
			{
				$('#mailName').attr('readonly',false);
				$('#mailText').attr('readonly',false);
				$(".mailNewMail").each( function () {
					$(this).show();
				});
				
				$(".mailViewMail").each( function () {
					$(this).hide();
				});
				
				var countries = listCountries(true);
				var s = $('#mailCountriesSelect');
				for (var country in countries)
				{
					var o = $('<option> </option>').text(country);
					o.attr('id', 'mailCountrySelect' + country);
					s.append(o);
				}
			}
			else if (style == 'reply')
			{
				$('#mailName').attr('readonly',false);
				$('#mailText').attr('readonly',false);
				$("#mailSenderLabel").hide();
				$("#mailSender").hide();
				$('#mailCountriesSelect').attr('style','display: ;');
				$('#mailCountriesSelectLabel').attr('style','display: ;');
				$('#mailLeftArrowButton').attr('style','display: ;');
				$('#mailRightArrowButton').attr('style','display: ;');
				$('#mailSendButton').attr('style','display: ;');
				$('#mailReplyButton').hide();
				$('#mailReplyAllButton').hide();
			}
			
			views.mail.showing = -1;
		},
		showMessage: function(id, reply)
		{
			if (id in state.d.mail)
			{
				if (reply == 'reply' || reply == 'replyAll')
				{

					views.mail.viewStyle('reply');
					views.mail.showing = -1;
					$('#mailName').val('RE:' + state.d.mail[id].name);
					$('#mailText').val("\n=========================\n" + state.d.mail[id].text);
					$('#mailText').focus();
					$('#mailText').selectRange(0, 0);
				}
				else
				{
					views.mail.viewStyle('read');
					views.mail.showing = id;
					$('#mailName').val(state.d.mail[id].name);
					$('#mailSender').text(state.d.mail[id].countrySender);
					$('#mailText').val(state.d.mail[id].text);
				}

				
					var countries = listCountries(true);
					var countriesSelect = $('#mailCountriesSelect');
					var recipientsSelect = $('#mailRecipientsSelect');
					var oldTargets = hashify(state.d.mail[id].countryTargets)
					for (var country in countries)
					{
						var o = $('<option> </option>').text(country);
						o.attr('id', 'mailCountrySelect' + country);
						if ((country in oldTargets) && (reply != 'reply')) 
						{

							recipientsSelect.append(o);
						}
						else
						{
							countriesSelect.append(o);
						}
					}
					if (reply == 'reply' || reply == 'replyAll')
					{
						//move self to non-recipients, then move sender to recipients
						$('#mailCountriesSelect').append($('#mailCountrySelect' + gameData.self.country));
						$('#mailRecipientsSelect').append($('#mailCountrySelect' + state.d.mail[id].countrySender));
					}
			}
			
		},
		showing: -1,		//the ID of the currently showing treaty, -1 if a treaty that isn't in the state object is showing (useful for know which treaty is being signed)
		refreshMultiList: function() 
		{
			var option = $('#mailListSelect > option:selected').first().text();				
			$('#mailMultiSelect').empty();
			
			for (var id in state.d.mail) //populate the multiSelect list
			{
				views.mail.add(id);
			}
			
			if (option == 'Read')
			{
				$('#mailListMarkUnreadButton').show();
			}
			else
			{
				$('#mailListMarkUnreadButton').hide();
			}
		},
		update: function(action, type, id, field, value) {
			var option = $('#mailListSelect > option:selected').first().text();		
			if (type == 'mail')
			{
				var targetCountries = hashify(state.d.mail[id].targets);
				if (gameData.self.country in targetCountries) //not really a problem for normal users, but this if statement keeps the admin from getting bombarded.
				{ 
					if (action == 'update')
					{
						views.mail.add(id);
						if ($('#mail').is(':hidden'))
						{
							$('#mailbutton').addClass('unreadmail');
						}
						if (state.d.mail[id].userSender == gameData.self.name)
						{
							chatAdd('err', 'Message Sent');
							var inView = views.mail.fromView();
							if ((inView.name == state.d.mail[id].name) && (state.d.mail[id].text.indexOf(inView.text) != -1) && (inView.countryTargets.length == state.d.mail[id].countryTargets.length) )
							{
								var match = true
								for (var recipient in inView.countryTargets)
								{
									if (inView.countryTargets[recipient] != state.d.mail[id].countryTargets[recipient])
									{
										match = false;
									}
								}
								if (match == true)
								{ //if its the message that was just sent, clear it
									views.mail.showing = -1;
									views.mail.viewStyle('new');
								}
							}
						}
					}
					else if (action == 'updateField') 
					{
						// if(id == views.mail.showing) //if the currently showing mail is updated, just refresh the view
						// {
							// views.mail.showTreaty(id);
						// }
						
						if (field == 'readBy')
						{
							if (option == 'Unread' && (gameData.self.name in hashify(state.d.mail[id].readBy)))
							{
								$('#csMail'+String(id)).remove();
							}
							else if (option == 'Read' && !(gameData.self.name in hashify(state.d.mail[id].readBy)))
							{
								$('#csMail'+String(id)).remove();
							}
						}
					}
					else if (action == 'remove')
					{
						//This doesn't seem likely to happen during a turn, so I'm not going to worry about it for now...
					}
				}
			}
		},
		activate: function ()
		{
			$('#mailbutton').removeClass('unreadmail');
		},
		fromView: function() { //generates a mail record object from the data in the form
			//(sender, targets, countrySender, countryTargets, name, text)
			var mail = new csMail(gameData.self.abbr, [], gameData.self.country, [], $('#mailName').val(), $('#mailText').val())
			
			$("#mailRecipientsSelect option").each(function () {
				//mail.targets.push($(this).text());
				mail.countryTargets.push($(this).text());
			});
			
			return mail;
		}
	},
	downloaddata:
	{
		initialize: function() {
			$('#downloaddataDownloadButton').click(downloaddataDownloadButton);
			
			fillTurnSelect('downloaddataTurn', gameData.game.turn);
			
			
			function downloaddataDownloadButton()
			{
				if ($('#downloaddataTurn option:selected').text() == gameData.game.turn)
				{
					views.downloaddata.showState(state.d);
				}
				else
				{
					socketOut('downloadStateData',{turn: $('#downloaddataTurn option:selected').text()});
				}
				//$("#downloaddataText").val(JSON.stringify(state.d));
			}
		},
		showState: function(pstate) {
			var countrySpecificRecords =['cover','resource','policy','tax','military','mail'];
			var sharedRecords = ['treaty','relation','map'];
			var unpreservedRecords = ['trade','log'];
			
			var countries = listCountries(true) //hash where keys are country names including admin
			
			$("#downloaddataText").val(''); //clear  the data field
			//console.log(pstate);
			
			/////// country specific records //////////
			var icountry = gameData.self.country;
			$("#downloaddataText").val($("#downloaddataText").val() + "\n" + icountry); //print country name
			for (var irecordType in countrySpecificRecords)
			{
				if (countrySpecificRecords[irecordType] in pstate)
				{
					$("#downloaddataText").val($("#downloaddataText").val() + "\n@\t" + countrySpecificRecords[irecordType]);
					
					var fieldsPrinted = false;
					for (var irecord in pstate[countrySpecificRecords[irecordType]])
					{
						var targets = hashify(pstate[countrySpecificRecords[irecordType]][irecord].targets,1); //hash of targets of the record
						if (icountry in targets)
						{
							if (!fieldsPrinted)  //if fields haven't been printed yet, print the fields
							{
								fieldsPrinted = true;
								for (var ifield in pstate[countrySpecificRecords[irecordType]][irecord])
								{
									$("#downloaddataText").val($("#downloaddataText").val() + "\t" + ifield);
								}
							}
							$("#downloaddataText").val($("#downloaddataText").val() + "\n#\t" + irecord); //print record ID
							for (var ifield in pstate[countrySpecificRecords[irecordType]][irecord])
							{
								$("#downloaddataText").val($("#downloaddataText").val() + "\t" + JSON.stringify(pstate[countrySpecificRecords[irecordType]][irecord][ifield]));
							}
						}
					}
				}
			}
			
			/////// shared records //////////
			$("#downloaddataText").val($("#downloaddataText").val() + "\n" + 'shared records'); //print 'shared records'
			for (var irecordType in sharedRecords)
			{
				if (sharedRecords[irecordType] in pstate)
				{
					$("#downloaddataText").val($("#downloaddataText").val() + "\n@\t" + sharedRecords[irecordType]);
					
					var fieldsPrinted = false;
					for (var irecord in pstate[sharedRecords[irecordType]])
					{
						var targets = hashify(pstate[sharedRecords[irecordType]][irecord].targets,1); //hash of targets of the record
						if (!fieldsPrinted)  //if fields haven't been printed yet, print the fields
						{
							fieldsPrinted = true;
							for (var ifield in pstate[sharedRecords[irecordType]][irecord])
							{
								$("#downloaddataText").val($("#downloaddataText").val() + "\t" + ifield);
							}
						}
						$("#downloaddataText").val($("#downloaddataText").val() + "\n#\t" + irecord);
						for (var ifield in pstate[sharedRecords[irecordType]][irecord])
						{
							$("#downloaddataText").val($("#downloaddataText").val() + "\t" + JSON.stringify(pstate[sharedRecords[irecordType]][irecord][ifield]));
						}
					}
				}
			}
			
			
			/////// unpreserved records //////////
			$("#downloaddataText").val($("#downloaddataText").val() + "\n" + 'other records'); //print 'shared records'
			for (var irecordType in unpreservedRecords)
			{
				if (unpreservedRecords[irecordType] in pstate)
				{
					$("#downloaddataText").val($("#downloaddataText").val() + "\n@\t" + unpreservedRecords[irecordType]);
					
					var fieldsPrinted = false;
					for (var irecord in pstate[unpreservedRecords[irecordType]])
					{
						var targets = hashify(pstate[unpreservedRecords[irecordType]][irecord].targets,1); //hash of targets of the record
						if (!fieldsPrinted)  //if fields haven't been printed yet, print the fields
						{
							fieldsPrinted = true;
							for (var ifield in pstate[unpreservedRecords[irecordType]][irecord])
							{
								$("#downloaddataText").val($("#downloaddataText").val() + "\t" + ifield);
							}
						}
						$("#downloaddataText").val($("#downloaddataText").val() + "\n#\t" + irecord);
						for (var ifield in pstate[unpreservedRecords[irecordType]][irecord])
						{
							$("#downloaddataText").val($("#downloaddataText").val() + "\t" + JSON.stringify(pstate[unpreservedRecords[irecordType]][irecord][ifield]));
						}
					}
				}
			}
			
		},
		update: function(action, type, id, field, value) {
			if (action == 'downloadStateData')
			{
				views.downloaddata.showState(value);
			}
		}
	},
	settings:
	{
		initialize: function() {
		
		},
		update: function(action, type, id, field) {
		
		}
	}
};