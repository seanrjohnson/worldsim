var sio; //socket
var state = {}; // the almighty State object
var gameData = {}; //stores information from the server that is not appropriate for the State object.  
// gameData.users = {username: {'playername': ... , 'country': ..., 'abbr': ..., 'admin': ..., 'online': ...}}
// gameData.self = {'country': ... , 'username': ... , 'abbr': ...}


var errors = [];

$(document).ready(function(){
	tryConnect();
});

// must go outside the function, otherwise internet explorer will interpret them wrong...
var hostname = document.location.hostname; // might have to change to document.location.host, not sure if port is important
var protocol = document.location.protocol;

function tryConnect()
{

	var secure;
	if (protocol == 'https:')
	{
		secure = true;
	}
	else
	{
		secure = false;
	}
	

	sio = io.connect(hostname, {'secure': secure});

	sio.socket.on('error', function (reason){
		console.error('Unable to connect Socket.IO', reason);
	});
	
	sio.on('connect', function (data){
		$('title').text('WorldSim');
		//$('#pageTitle').text('');
		//$('#pageHead').text('');
		state = new wsClientState;
	});
	
	sio.on('disconnect', function (data){
		if (data == 'booted') //wish there was a way to send a more specific message...
		{
			document.location = '/index.html?r=3';
		}
		else
		{
			console.log(data);
			console.log('connection Lost');
			//warn client, via the chat box, that the connection has been lost
		}
	});
	socketIn(sio);  //inialize the rest of the functions
}