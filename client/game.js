function initializeLayout() {
	//$('#chatArea').hide();
	$("#logoutButton").click(function () {
		sio.emit('logout');
	});
	
	var links1 = $("#topBar ul li a");
	
	for (var i = 0; i<links1.length; i++){
		inside1(i);
	}
	function inside1 (index)
	{
		$(links1[index]).click(function () {
			var viewtext =$(links1[index]).text().toLowerCase().replace(/\s/g, ""); 
			if ($("#"+viewtext+"button").hasClass("selected")){
				displayView(viewtext, false);
			} else {
				displayView(viewtext,true);
			}
		});
	}
}

// $(window).resize(function(){
	// //fix textareas here
// });

/////////// views code that is shared between admin and player /////////
function initializeViews(){
	$('#main').show();
	$('#rightArea').show();
	$('#leftArea').show();
	for (var key in views)
	{
		views[key].initialize();
	}
}

function updateViews(action, type, id, field, value) 
{
	for (var key in views)
	{
		views[key].update(action, type, id, field, value);
	}
}

//moved over from game.js, originally was in ws.js (it'll find a home someday...)
//var activeView = {};
function displayView(view,on) //view is the name of the view to display, on is a bool => true if you want the view to show, false if you want it to be hidden
{
	//console.log(view);
	//activeView = view;

	if (view in views)
	{
		if (on){
			if ($('#' + view).parent().attr("id")=="main"){
				$('#main').prepend($('#'+view));
				$('#main > div').hide();
				$('#main > div').each(function() {
					$("#"+this.id+"button").removeClass("selected");
				});
			} else{
				$("#"+view+"button").addClass("toggled");
			}
			$("#"+view+"button").addClass("selected");
			$('#' + view).show();
			if ('activate' in views[view])
			{
				views[view].activate();
			}
		} else{
			$("#"+view+"button").removeClass("selected");
			$("#"+view+"button").removeClass("toggled");
			$('#' + view).hide();
		}
	}
}

function fillTurnSelect(id, maxTurn)
{
	if (maxTurn === undefined)
	{
		maxTurn = gameData.game.turn;
	}
	for (var i = 1; i <= maxTurn; i++)
	{
		$('#' + id).append($('<option/>').text(String(i)));
	}
	$('#' + id).val(gameData.game.turn); //select the current turn
}

//////////// other functions  //////////////
function listCountries(includeAdmin)
{
	var out = {};
	
	if (includeAdmin === undefined)
	{
		includeAdmin = false;
	}
	
	for(var key in gameData.users)
	{
		if (gameData.users[key].admin === false || includeAdmin)
		{
			out[gameData.users[key].country] = 1;
		}
	}
	return out;
}

function countryFromAbbreviation(abbr)
{
	var out = null;
	for (var name in gameData.users)
	{
		if (gameData.users[name].abbr == abbr)
		{
			out = gameData.users[name].country
		}
	}
	return out;
}

function userFromAbbreviation(abbr)
{
	var out = null;
	for (var name in gameData.users)
	{
		if (gameData.users[name].abbr == abbr)
		{
			out = name;
		}
	}
	return out;
}

function usersFromCountry(country)
{
	var out = [];
	for (var name in gameData.users)
	{
		if (gameData.users[name].country == country)
		{
			out.push(name);
		}
	}
	return out;
}


////// added so it works better in IE 9 (might not actually be helpful, I can't really tell)
///// Taken from http://whattheheadsaid.com/2010/10/a-safer-object-keys-compatibility-implementation
//
Object.keys = Object.keys || (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !{toString:null}.propertyIsEnumerable("toString"),
        DontEnums = [
            'toString',
            'toLocaleString',
            'valueOf',
            'hasOwnProperty',
            'isPrototypeOf',
            'propertyIsEnumerable',
            'constructor'
        ],
        DontEnumsLength = DontEnums.length;
 
    return function (o) {
        if (typeof o != "object" && typeof o != "function" || o === null)
            throw new TypeError("Object.keys called on a non-object");
 
        var result = [];
        for (var name in o) {
            if (hasOwnProperty.call(o, name))
                result.push(name);
        }
 
        if (hasDontEnumBug) {
            for (var i = 0; i < DontEnumsLength; i++) {
                if (hasOwnProperty.call(o, DontEnums[i]))
                    result.push(DontEnums[i]);
            }
        }
 
        return result;
    };
})();

//// from http://stackoverflow.com/questions/13425363/jquery-set-textarea-cursor-to-end-of-text
$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};